package org.nrg.xnat.workflow.listeners;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.model.WrkXnatexecutionenvironmentParameterI;
import org.nrg.xdat.om.FsFsdata;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.WrkXnatexecutionenvironment;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.event.entities.WorkflowStatusEvent;
import org.nrg.xnat.event.listeners.PipelineEmailHandlerAbst;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FreesurferSnapshotsEmailHandler extends PipelineEmailHandlerAbst {
    final static Logger logger = Logger.getLogger(FreesurferSnapshotsEmailHandler.class);

    private final String PIPELINE_NAME = "Freesurfer/Freesurfer_Snapshots.xml";
    private final String PIPELINE_NAME_PRETTY = "Freesurfer Snapshots";

    private static List<Integer> SENT_ALREADY= Lists.newArrayList();

    public void handleEvent(WorkflowStatusEvent e) {
        if (!(e.getWorkflow() instanceof WrkWorkflowdata)) {
            return;
        }
        WrkWorkflowdata wrk = (WrkWorkflowdata)e.getWorkflow();
        Map<String,Object> params = Maps.newHashMap();
        params.put("pipelineName",PIPELINE_NAME_PRETTY);
        if (completed(e)) {
            FSSnapEmailImpl(e, wrk, PIPELINE_NAME, DEFAULT_TEMPLATE_SUCCESS, "Snapshots were generated for", "processed.lst", params);
        } else if (failed(e)) {
            FSSnapEmailImpl(e, wrk, PIPELINE_NAME, DEFAULT_TEMPLATE_FAILURE, DEFAULT_SUBJECT_FAILURE, "processed.lst", params);
        }
    }

    public void FSSnapEmailImpl(final WorkflowStatusEvent e, WrkWorkflowdata wrk, final String pipelineName, final String template, final String subject, final String notificationFileName, Map<String,Object> params){

        try {
            String _pipelineName = wrk.getPipelineName();
            if (_pipelineName==null) {
                _pipelineName = (String) PoolDBUtils.ReturnStatisticQuery("select pipeline_name from wrk_workflowdata where wrk_workflowdata_id=" + wrk.getWrkWorkflowdataId(), "pipeline_name", null, null);
            }
            if(StringUtils.endsWith(_pipelineName, pipelineName) && (completed(e) || failed(e)) && !SENT_ALREADY.contains(wrk.getWrkWorkflowdataId())) {
                if ( wrk.getPipelineName() == null) {
                    wrk = WrkWorkflowdata.getWrkWorkflowdatasByWrkWorkflowdataId(wrk.getWrkWorkflowdataId(),wrk.getUser(),false);
                }
                SchemaElement objXsiType;
                try {
                    objXsiType = SchemaElement.GetElement(wrk.getDataType());
                } catch (Throwable e1) {
                    logger.error("", e1);//this shouldn't happen
                    return;
                }


                if (objXsiType.getGenericXFTElement().instanceOf("fs:fsData") && wrk.getId() != null) {
                    final FsFsdata fs = FsFsdata.getFsFsdatasById(wrk.getId(), wrk.getUser(), false);

                    params.put("justification", wrk.getJustification());

                    if (fs != null) {
                        String _subject;

                        params.put("assessorId",fs.getId());
                        XnatExperimentdata expt = XnatExperimentdata.getXnatExperimentdatasById(fs.getImagesessionId(),wrk.getUser(),false);

                        if(failed(e)) {
                            // Find pipeline logs
                            String builddir = null;
                            String label = null;
                            Map<String,File> attachments = Maps.newHashMap();
                            for (WrkXnatexecutionenvironmentParameterI pipelineParameter : ((WrkXnatexecutionenvironment)wrk.getExecutionenvironment()).getParameters_parameter()) {
                                if ("builddir".equals(pipelineParameter.getName())) {
                                    builddir = pipelineParameter.getParameter();
                                } else if ("label".equals(pipelineParameter.getName())) {
                                    label = pipelineParameter.getParameter();
                                }
                            }
                            if (StringUtils.isNotBlank(builddir) && StringUtils.isNotBlank(label)) {
                                String logPath = builddir + "/" + label + "/LOGS/";
                                File logDirFileObj = new File(logPath);
                                if (logDirFileObj.exists()) {
                                    File[] logFileObjs = logDirFileObj.listFiles();
                                    if (logFileObjs != null) {
                                        for (File logFileObj : logFileObjs ) {
                                            if (logFileObj.getName().endsWith(".log") || logFileObj.getName().endsWith(".err")) {
                                                attachments.put(logFileObj.getName(), logFileObj);
                                            }
                                        }
                                    }
                                }
                            }
                            params.put("attachments", attachments);
                            _subject = TurbineUtils.GetSystemName()+" update: Processing failed for " + fs.getLabel() +" "+subject;

                        } else {
                            _subject = TurbineUtils.GetSystemName()+" update: " +subject+" "+fs.getLabel();
                        }
                        send(e, wrk, expt, params, template, _subject, notificationFileName, new ArrayList<String>());
                    }
                }

                SENT_ALREADY.add(wrk.getWrkWorkflowdataId());
            }
        } catch (Throwable e1) {
            logger.error("",e1);
        }
    }
}
