package org.nrg.xnat.generated.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;

@XnatPlugin(value = "nrg_webapp_freesurfercommon", name = "XNAT 1.7 FreeSurfer Common Plugin", description = "This is the XNAT 1.7 FreeSurfer Common Plugin.",
        dataModels = {@XnatDataModel(value = "fs:fsData",
                singular = "Freesurfer",
                plural = "Freesurfers")})
public class FreesurferCommonPlugin {
}