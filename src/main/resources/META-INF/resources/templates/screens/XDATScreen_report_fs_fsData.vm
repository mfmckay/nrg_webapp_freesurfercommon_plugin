<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
$page.setTitle("Freesurfer Analysis Details")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#if ($data.getParameters().getString("popup"))
    #set ($popup = $data.getParameters().getString("popup") )
    #set ($popup = "false")
#end

<script type="text/javascript" src="/scripts/FileViewer.js"></script>
<script type="text/javascript" src="/scripts/BasePopup.js"></script>
<script type="text/javascript" src="/scripts/restSharer.js"></script>

<script type="text/javascript">
    var obj=new Object();
    obj.uri=serverRoot + "/REST/projects/$!om.getProject()/subjects/$!om.SubjectData.getLabel()/experiments/$!om.getId()";
    obj.uri=serverRoot + "/REST/experiments/${om.getImageSessionData().getId()}/assessors/$!om.getId()";

    #if($item.isActive() || $item.isQuarantine())
        #if ($item.canEdit($data.getSession().getAttribute("user")))
            obj.canEdit=true;
        #else
            obj.canEdit=false;
        #end
        #if ($item.canDelete($data.getSession().getAttribute("user")))
            obj.canDelete=true;
        #else
            obj.canDelete=false;
        #end
    #else
        obj.canEdit=false;
        obj.canDelete=false;
    #end

    obj.catalogs=new Object();
    obj.catalogs.ids=new Array();
    window.viewer=new FileViewer(obj);

    function showFiles(){
        window.viewer.render();
    }
</script>
<script>
    jq(document).ready(function(){
        function toggleLI(_name){
            var elements = document.getElementsByName("li_"+_name);
            for(var trI=0;trI<elements.length;trI++){
                if(elements[trI].style.display=="none"){
                    elements[trI].style.display="block";
                }else{
                    elements[trI].style.display="none";
                }
            }
        }

        // initialize tabs
        function summaryIndexChanged(){
            var activeIndex=this.get("activeIndex");
            YAHOO.util.Cookie.set("${project.getId()}.summary.index",activeIndex);
        }
        function summaryTabManagerInit(){
            window.summaryTabView = new YAHOO.widget.TabView('exptSummary');

            window.expt_summary_module = new YAHOO.widget.Module("expt_summary_module",{visible:false,zIndex:5});

            window.expt_summary_module.show();

            var tabIndex=YAHOO.util.Cookie.get("${om.getId()}.summary.index");
            window.summaryTabView.set('activeIndex',tabIndex||0);

            window.summaryTabView.subscribe("activeTabChange",summaryIndexChanged);
        }
        summaryTabManagerInit();

        function analysisIndexChanged(){
            var activeIndex=this.get("activeIndex");
            YAHOO.util.Cookie.set("${project.getId()}.analysis.index",activeIndex);
        }
        function analysisTabManagerInit(){
            window.analysisTabView = new YAHOO.widget.TabView('analysisTabs');

            window.analysis_tab_module = new YAHOO.widget.Module("analysis_tab_module",{visible:false,zIndex:5});

            window.analysis_tab_module.show();

            var tabIndex=YAHOO.util.Cookie.get("${om.getId()}.analysis.index");
            window.analysisTabView.set('activeIndex',tabIndex||0);

            window.analysisTabView.subscribe("activeTabChange",analysisIndexChanged);
        }
        analysisTabManagerInit();
    });
</script>


#addCustomScreens($om.getXSIType() "report/alert")
#parse("/screens/workflow_alert.vm")

<h2>Freesurfer QC Report: $!om.label</h2>
<table>
    <tr valign="top">
        <td name="headerTabset" width="75%">
            <div id="exptSummary" class="yui-navset" style="margin-right: 80px;">
                <ul class="yui-nav" style="">
                    <li class="selected"><a href="#tab1"><em>Validation</em></a></li>
                    <li><a href="#tab2"><em>Details</em></a></li>
                    <li><a href="#tab3"><em>Project Sharing</em></a></li>

                </ul>
                <div class="yui-content">
                    <div id="tab1">
                        #if($!item.getStringProperty("fs:fsData/validation/status"))
                            <table>
                                <tr><th>Validation Status: </th><td>$!item.getStringProperty("fs:fsData/validation/status")</td></tr>
                                <tr><th>Validated By: </th><td>$!item.getStringProperty("fs:fsData/validation/validated_by")</td></tr>
                                <tr><th>Date Validated: </th><td>$!item.getProperty("fs:fsData/validation/date")</td></tr>
                                <tr><th>Method: </th><td>$!item.getStringProperty("fs:fsData/validation/method")</td></tr>
                                <tr><th>Notes: </th><td>$!item.getStringProperty("fs:fsData/validation/notes")</td></tr>
                            </table>
                        #else
                            <p>QC Assessor requires validation. </p>
                        #end
                        <p style="margin-top: 1em;">
                            #set ($url = "$link.setPage('Fs_fsData_QC_64.vm').addPathInfo('search_value',$item.getProperty('ID')).addPathInfo('search_element','fs:fsData').addPathInfo('search_field','fs:fsData.ID')")
                            <button class="btn btn2" onclick="return popupWithProperties('$url','','width=950,height=600,status=yes,resizable=yes,scrollbars=yes')">View Snapshots</button>
                            <button class="btn btn2" onclick="window.location.assign('/app/action/XDATActionRouter/xdataction/edit/search_element/fs%3AfsData/search_field/fs%3AfsData.id/search_value/$!item.getStringProperty("fs:fsData/ID")/popup/false/project/$om.getProject()')">Set QC Status</button>
                        </p>
                    </div>
                    <div id="tab2">
                        <table class="xnat-table">
                            #set ($id=".ID")
                            #set ($field="$om.getImageSessionData().getXSIType()$id")

                            #set ($url = "$link.setAction('DisplayItemAction').addPathInfo('search_element',$om.getImageSessionData().getXSIType()).addPathInfo('search_field',$field).addPathInfo('search_value',$om.getImageSessionData().getId()).addPathInfo('project',$om.getProject())")

                            <TR><TD>MRSession</TD><TD><a href="$url">$om.getImageSessionData().getLabel()</a></TD></TR>
                            <TR><TD>Date</TD><TD>$!om.date</TD></TR>
                            #if ($!item.getStringProperty("fs:fsData/note")!="")
                                <TR><TD>Note</TD><TD>$!item.getStringProperty("fs:fsData/note")</TD></TR>
                            #end
                            <TR><TD>ID</TD><TD>$!om.label</TD></TR>
                            <TR><TD>Project</TD><TD>$!om.project</TD></TR>
                            <TR><TD>Freesurfer version</TD><TD>$!om.getFsVersion()</TD></TR>
                        </table>
                    </div>
                    <div id="tab3">
                        #parse($turbineUtils.getTemplateName("sharing",$om.getXSIType(),$!project))
                    </div>
                </div>
            </div>

        </td>
        <td name="headerActions" width="25%" valign="top">
            #parse($turbineUtils.getTemplateName("actions",$om.getXSIType(),$project))
        </td>
    </tr>
</table>

<h2>Freesurfer Analysis</h2>

<div id="analysisTabs" class="yui-navset">
    <ul class="yui-nav" style="">
        <li class="selected"><a href="#analysis1"><em>Subcortical Segmentation</em></a></li>
        <li><a href="#analysis2"><em>Surface Measures</em></a></li>
        #if($turbineUtils.templateExists("/screens/fs_ilp.vm"))
            <li><a href="#ILPanalysis"><em>ILP</em></a></li>
        #end
        <li><a href="#analysis3"><em>Input Parameters</em></a></li>
    </ul>
    <div class="yui-content">
        <div id="analysis1">

            <table class="xnat-table">
                <TR><TD>ICV</TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/icv"),0)</TD></TR>
                <TR><TD>Left hemi. cortical gray matter vol.</TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/lhcortexvol"),0)</TD></TR>
                <TR><TD>Right hemi. cortical gray matter vol.</TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/rhcortexvol"),0)</TD></TR>
                <TR><TD>Total cortical gray matter vol. (based on surface-stream) </TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/cortexvol"),0)</TD></TR>
                <TR><TD>Subcortical gray matter vol.</TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/subcortgrayvol"),0)</TD></TR>
                <TR><TD>Total gray matter vol.</TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/totalgrayvol"),0)</TD></TR>
                <TR><TD>Supratentorial vol.</TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/supratentorialvol"),0)</TD></TR>
                <TR><TD>Left hemi. cortical white matter vol.</TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/lhcorticalwhitemattervol"),0)</TD></TR>
                <TR><TD>Right hemi. cortical white matter vol.</TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/rhcorticalwhitemattervol"),0)</TD></TR>
                <TR><TD>Total cortical white matter vol.</TD><TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/corticalwhitemattervol"),0)</TD></TR>
            </table>

            <!-- BEGIN fs_fsData VOLUMETRIC MEASURES -->
            #set($fs_fsData_region_11_NUM_ROWS=$item.getChildItems("fs:fsData/measures/volumetric/regions/region").size() - 1)
            #if($fs_fsData_region_11_NUM_ROWS>=0)
                <table class="xnat-table">

                    <tr><TH>Region Name</TH><TH>Seg Id</TH><TH>Hemisphere</TH><TH>NVoxels</TH><TH>Volume</TH><TH>Mean</TH><TH>Range</TH></tr>

                    #foreach($fs_fsData_region_11_COUNTER in [0..$fs_fsData_region_11_NUM_ROWS])
                        <!-- BEGIN fs_fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER] -->
                        <tr>
                            <TD>$!item.getStringProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/name")</TD>
                            <TD>$!item.getStringProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/SegId")</TD>
                            <TD>$!item.getStringProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/hemisphere")</TD>
                            <TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/NVoxels"),0)</TD>
                            <TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/Volume"),0)</TD>
                            <TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normMean"),4) +-$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normStdDev"),4)</TD>
                            <TD>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normRange"),0)&nbsp;($!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normMin"),0)-$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normMax"),0))</TD>

                        </tr>


                        <!-- END fs_fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER] -->
                    #end
                </table>
            #end
            <!-- END fs_fsData VOLUMETRIC MEASURES-->

        </div>

        <div id="analysis2">
            <!-- BEGIN fs_fsData SURFACE MEASURES-->

            #set($fs_fsData_hemisphere_12_NUM_ROWS=$item.getChildItems("fs:fsData/measures/surface/hemisphere").size() - 1)
            #if($fs_fsData_hemisphere_12_NUM_ROWS>=0)
                #set ($hemis = $om.getMeasures_surface_hemisphere())
                #set ($hemis_NUM_ROWS = $hemis.size() - 1)
                #foreach($fs_fsData_hemisphere_COUNTER in [0..$hemis_NUM_ROWS])
                    #set ($hemi = $hemis.get($fs_fsData_hemisphere_COUNTER))
                    #set ($hemi_regions = $hemi.getRegions_region())
                    #set ($hemi_regions_NUM_ROWS = $hemi_regions.size() - 1)
                    <table class="xnat-table">
                        <tr><TH align="left" colspan="9">Hemisphere: $hemi.getName() NumVert: $!turbineUtils.formatNumber($hemi.getNumvert(),0) SurfArea: $!turbineUtils.formatNumber($hemi.getSurfarea(),0)</TH></TR>
                        <TR><TH>Region Name</TH><TH>NumVert</TH><TH>SurfArea</TH><TH>GrayVol</TH><TH>Thickness</TH><TH>MeanCurv</TH><TH>GausCurv</TH><TH>FoldInd</TH><TH>CurvInd</TH></tr>

                        #foreach($fs_fsData_hemisphere_regions_COUNTER in [0..$hemi_regions_NUM_ROWS])
                            #set ($region = $!hemi_regions.get($fs_fsData_hemisphere_regions_COUNTER))
                            <tr>
                                <TD> $!region.getName()</TD>
                                <TD>$!turbineUtils.formatNumber($!region.getNumvert(),0)</TD>
                                <TD>$!turbineUtils.formatNumber($!region.getSurfarea(),0)</TD>
                                <TD>$!turbineUtils.formatNumber($!region.getGrayvol(),0)</TD>
                                <TD>$!turbineUtils.formatNumber($!region.Thickavg,3) +-$!turbineUtils.formatNumber($!region.Thickstd,3)</TD>
                                <TD>$!turbineUtils.formatNumber($!region.getMeancurv(),3)</TD>
                                <TD>$!turbineUtils.formatNumber($!region.getGauscurv(),3)</TD>
                                <TD>$!turbineUtils.formatNumber($!region.getFoldind(),3)</TD>
                                <TD>$!turbineUtils.formatNumber($!region.getCurvind(),3)</TD>
                            </tr>
                        #end
                    </table>
                #end
            #end
            <!-- END fs_fsData SURFACE MEASURES-->

        </div>

        #if($turbineUtils.templateExists("/screens/fs_ilp.vm"))
            <div id="ILPanalysis">
                #parse("/screens/fs_ilp.vm")
            </div>
        #end

        <div id="analysis3">
            <!-- BEGIN fs_fsData Input Parameters-->
            #set ($params = $om.getParameters_addparam())
            #if ($params.size() > 0)
                <table class="xnat-table">
                    <tr> <TH>Name </TH> <TH>Value</TH></TR>
                    #foreach ($param in $params)
                        <TR> <td> $param.getName() </td> <td> $param.getAddfield() </td> </tr>
                    #end
                </table>
            #end
            <!-- END fs_fsData Input Parameters-->
        </div>
    </div>

</div>

<div style="margin: 3em 0;">
#parse("/screens/workflow_summary.vm")
</div>



<!-- BEGIN fs_fsData Recon all-->

<div style="margin: 3em 0;">
    #set ($scanCounter = 3)
    <h3 name="LINK${scanCounter}" onClick=" return blocking($scanCounter);">
        <img ID="IMG$scanCounter" src="$content.getURI("images/plus.jpg")" border=0>
        Log files
    </h3>


    <div id="span$!scanCounter" style="position:relative; display:none;">
        <ul>
            <li>
                <a name="LINK${scanCounter}" href="$content.getURI("/REST/experiments/$om.getImageSessionData().getId()/assessors/$om.getId()/resources/DATA/files/$om.getImageSessionData().getLabel()/scripts/recon-all.log")" >
                    Recon All
                </a>
            </li>
        </ul>
        <!-- END fs_fsData Input Parameters-->
    </div>
</div>
