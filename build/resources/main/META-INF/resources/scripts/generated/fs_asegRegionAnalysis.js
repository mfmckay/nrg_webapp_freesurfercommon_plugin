/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:47 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function fs_asegRegionAnalysis(){
this.xsiType="fs:asegRegionAnalysis";

	this.getSchemaElementName=function(){
		return "asegRegionAnalysis";
	}

	this.getFullSchemaElementName=function(){
		return "fs:asegRegionAnalysis";
	}
this.extension=dynamicJSLoad('xnat_mrAssessorData','generated/xnat_mrAssessorData.js');

	this.Brainmasknvox=null;


	function getBrainmasknvox() {
		return this.Brainmasknvox;
	}
	this.getBrainmasknvox=getBrainmasknvox;


	function setBrainmasknvox(v){
		this.Brainmasknvox=v;
	}
	this.setBrainmasknvox=setBrainmasknvox;

	this.Brainmaskvol=null;


	function getBrainmaskvol() {
		return this.Brainmaskvol;
	}
	this.getBrainmaskvol=getBrainmaskvol;


	function setBrainmaskvol(v){
		this.Brainmaskvol=v;
	}
	this.setBrainmaskvol=setBrainmaskvol;

	this.Brainsegnvox=null;


	function getBrainsegnvox() {
		return this.Brainsegnvox;
	}
	this.getBrainsegnvox=getBrainsegnvox;


	function setBrainsegnvox(v){
		this.Brainsegnvox=v;
	}
	this.setBrainsegnvox=setBrainsegnvox;

	this.Brainsegvol=null;


	function getBrainsegvol() {
		return this.Brainsegvol;
	}
	this.getBrainsegvol=getBrainsegvol;


	function setBrainsegvol(v){
		this.Brainsegvol=v;
	}
	this.setBrainsegvol=setBrainsegvol;

	this.Icv=null;


	function getIcv() {
		return this.Icv;
	}
	this.getIcv=getIcv;


	function setIcv(v){
		this.Icv=v;
	}
	this.setIcv=setIcv;
	this.Regions_region =new Array();

	function getRegions_region() {
		return this.Regions_region;
	}
	this.getRegions_region=getRegions_region;


	function addRegions_region(v){
		this.Regions_region.push(v);
	}
	this.addRegions_region=addRegions_region;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="mrAssessorData"){
				return this.Mrassessordata ;
			} else 
			if(xmlPath.startsWith("mrAssessorData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Mrassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Mrassessordata!=undefined)return this.Mrassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="BrainMaskNVox"){
				return this.Brainmasknvox ;
			} else 
			if(xmlPath=="BrainMaskVol"){
				return this.Brainmaskvol ;
			} else 
			if(xmlPath=="BrainSegNVox"){
				return this.Brainsegnvox ;
			} else 
			if(xmlPath=="BrainSegVol"){
				return this.Brainsegvol ;
			} else 
			if(xmlPath=="ICV"){
				return this.Icv ;
			} else 
			if(xmlPath=="regions/region"){
				return this.Regions_region ;
			} else 
			if(xmlPath.startsWith("regions/region")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Regions_region ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Regions_region.length;whereCount++){

					var tempValue=this.Regions_region[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Regions_region[whereCount]);

					}

				}
				}else{

				whereArray=this.Regions_region;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="mrAssessorData"){
				this.Mrassessordata=value;
			} else 
			if(xmlPath.startsWith("mrAssessorData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Mrassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Mrassessordata!=undefined){
					this.Mrassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Mrassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Mrassessordata= instanciateObject("xnat:mrAssessorData");//omUtils.js
						}
						if(options && options.where)this.Mrassessordata.setProperty(options.where.field,options.where.value);
						this.Mrassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="BrainMaskNVox"){
				this.Brainmasknvox=value;
			} else 
			if(xmlPath=="BrainMaskVol"){
				this.Brainmaskvol=value;
			} else 
			if(xmlPath=="BrainSegNVox"){
				this.Brainsegnvox=value;
			} else 
			if(xmlPath=="BrainSegVol"){
				this.Brainsegvol=value;
			} else 
			if(xmlPath=="ICV"){
				this.Icv=value;
			} else 
			if(xmlPath=="regions/region"){
				this.Regions_region=value;
			} else 
			if(xmlPath.startsWith("regions/region")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Regions_region ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Regions_region.length;whereCount++){

					var tempValue=this.Regions_region[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Regions_region[whereCount]);

					}

				}
				}else{

				whereArray=this.Regions_region;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("fs:asegRegionAnalysis_region");//omUtils.js
					}
					this.addRegions_region(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="regions/region"){
			this.addRegions_region(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="regions/region"){
			return "http://nrg.wustl.edu/fs:asegRegionAnalysis_region";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="BrainMaskNVox"){
			return "field_data";
		}else if (xmlPath=="BrainMaskVol"){
			return "field_data";
		}else if (xmlPath=="BrainSegNVox"){
			return "field_data";
		}else if (xmlPath=="BrainSegVol"){
			return "field_data";
		}else if (xmlPath=="ICV"){
			return "field_data";
		}else if (xmlPath=="regions/region"){
			return "field_multi_reference";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<fs:ASEGRegionAnalysis";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</fs:ASEGRegionAnalysis>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Brainmasknvox!=null){
			xmlTxt+="\n<fs:BrainMaskNVox";
			xmlTxt+=">";
			xmlTxt+=this.Brainmasknvox;
			xmlTxt+="</fs:BrainMaskNVox>";
		}
		if (this.Brainmaskvol!=null){
			xmlTxt+="\n<fs:BrainMaskVol";
			xmlTxt+=">";
			xmlTxt+=this.Brainmaskvol;
			xmlTxt+="</fs:BrainMaskVol>";
		}
		if (this.Brainsegnvox!=null){
			xmlTxt+="\n<fs:BrainSegNVox";
			xmlTxt+=">";
			xmlTxt+=this.Brainsegnvox;
			xmlTxt+="</fs:BrainSegNVox>";
		}
		if (this.Brainsegvol!=null){
			xmlTxt+="\n<fs:BrainSegVol";
			xmlTxt+=">";
			xmlTxt+=this.Brainsegvol;
			xmlTxt+="</fs:BrainSegVol>";
		}
		if (this.Icv!=null){
			xmlTxt+="\n<fs:ICV";
			xmlTxt+=">";
			xmlTxt+=this.Icv;
			xmlTxt+="</fs:ICV>";
		}
			var child0=0;
			var att0=0;
			child0+=this.Regions_region.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<fs:regions";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Regions_regionCOUNT=0;Regions_regionCOUNT<this.Regions_region.length;Regions_regionCOUNT++){
			xmlTxt +="\n<fs:region";
			xmlTxt +=this.Regions_region[Regions_regionCOUNT].getXMLAtts();
			if(this.Regions_region[Regions_regionCOUNT].xsiType!="fs:asegRegionAnalysis_region"){
				xmlTxt+=" xsi:type=\"" + this.Regions_region[Regions_regionCOUNT].xsiType + "\"";
			}
			if (this.Regions_region[Regions_regionCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Regions_region[Regions_regionCOUNT].getXMLBody(preventComments);
					xmlTxt+="</fs:region>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</fs:regions>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Brainmasknvox!=null) return true;
		if (this.Brainmaskvol!=null) return true;
		if (this.Brainsegnvox!=null) return true;
		if (this.Brainsegvol!=null) return true;
		if (this.Icv!=null) return true;
			if(this.Regions_region.length>0)return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
