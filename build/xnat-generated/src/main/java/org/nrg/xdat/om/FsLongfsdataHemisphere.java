/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:48 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 *//*
 ******************************** 
 * DO NOT MODIFY THIS FILE HERE
 *
 * TO MODIFY, COPY THIS FILE to src/main/java/org/nrg/xdat/om/ and modify it there 
 ********************************/
@SuppressWarnings({"unchecked","rawtypes"})
public class FsLongfsdataHemisphere extends BaseFsLongfsdataHemisphere {

	public FsLongfsdataHemisphere(ItemI item)
	{
		super(item);
	}

	public FsLongfsdataHemisphere(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsLongfsdataHemisphere(UserI user)
	 **/
	public FsLongfsdataHemisphere()
	{}

	public FsLongfsdataHemisphere(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
