/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:48 CST 2017
 *
 */
package org.nrg.xdat.model;

import java.util.List;

/**
 * @author XDAT
 *
 */
public interface FsAparcregionanalysisHemisphereI {

	public String getXSIType();

	public void toXML(java.io.Writer writer) throws java.lang.Exception;

	/**
	 * @return Returns the NumVert.
	 */
	public Double getNumvert();

	/**
	 * Sets the value for NumVert.
	 * @param v Value to Set.
	 */
	public void setNumvert(Double v);

	/**
	 * @return Returns the SurfArea.
	 */
	public Double getSurfarea();

	/**
	 * Sets the value for SurfArea.
	 * @param v Value to Set.
	 */
	public void setSurfarea(Double v);

	/**
	 * regions/region
	 * @return Returns an List of org.nrg.xdat.model.FsAparcregionanalysisHemisphereRegionI
	 */
	public <A extends org.nrg.xdat.model.FsAparcregionanalysisHemisphereRegionI> List<A> getRegions_region();

	/**
	 * regions/region
	 * @return Returns an List of org.nrg.xdat.model.FsAparcregionanalysisHemisphereRegionI
	 */
	public <A extends org.nrg.xdat.model.FsAparcregionanalysisHemisphereRegionI> void addRegions_region(A item) throws Exception;

	/**
	 * @return Returns the name.
	 */
	public String getName();

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v);

	/**
	 * @return Returns the fs_aparcRegionAnalysis_hemisphere_id.
	 */
	public Integer getFsAparcregionanalysisHemisphereId();
}
