/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:47 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 *//*
 ******************************** 
 * DO NOT MODIFY THIS FILE HERE
 *
 * TO MODIFY, COPY THIS FILE to src/main/java/org/nrg/xdat/om/base/ and modify it there 
 ********************************/
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseFsAsegregionanalysisRegion extends AutoFsAsegregionanalysisRegion {

	public BaseFsAsegregionanalysisRegion(ItemI item)
	{
		super(item);
	}

	public BaseFsAsegregionanalysisRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAsegregionanalysisRegion(UserI user)
	 **/
	public BaseFsAsegregionanalysisRegion()
	{}

	public BaseFsAsegregionanalysisRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
