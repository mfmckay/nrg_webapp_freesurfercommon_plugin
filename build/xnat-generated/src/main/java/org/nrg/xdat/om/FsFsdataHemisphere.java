/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:47 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 *//*
 ******************************** 
 * DO NOT MODIFY THIS FILE HERE
 *
 * TO MODIFY, COPY THIS FILE to src/main/java/org/nrg/xdat/om/ and modify it there 
 ********************************/
@SuppressWarnings({"unchecked","rawtypes"})
public class FsFsdataHemisphere extends BaseFsFsdataHemisphere {

	public FsFsdataHemisphere(ItemI item)
	{
		super(item);
	}

	public FsFsdataHemisphere(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsFsdataHemisphere(UserI user)
	 **/
	public FsFsdataHemisphere()
	{}

	public FsFsdataHemisphere(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
