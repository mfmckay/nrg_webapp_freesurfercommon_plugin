/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:48 CST 2017
 *
 */
package org.nrg.xdat.model;

import java.util.List;

/**
 * @author XDAT
 *
 */
public interface FsFsdataI extends XnatImageassessordataI {

	public String getXSIType();

	public void toXML(java.io.Writer writer) throws java.lang.Exception;

	/**
	 * @return Returns the fs_version.
	 */
	public String getFsVersion();

	/**
	 * Sets the value for fs_version.
	 * @param v Value to Set.
	 */
	public void setFsVersion(String v);

	/**
	 * @return Returns the ilp_age.
	 */
	public Double getIlpAge();

	/**
	 * Sets the value for ilp_age.
	 * @param v Value to Set.
	 */
	public void setIlpAge(Double v);

	/**
	 * @return Returns the measures/volumetric/ICV.
	 */
	public Double getMeasures_volumetric_icv();

	/**
	 * Sets the value for measures/volumetric/ICV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_icv(Double v);

	/**
	 * @return Returns the measures/volumetric/lhCortexVol.
	 */
	public Double getMeasures_volumetric_lhcortexvol();

	/**
	 * Sets the value for measures/volumetric/lhCortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhcortexvol(Double v);

	/**
	 * @return Returns the measures/volumetric/rhCortexVol.
	 */
	public Double getMeasures_volumetric_rhcortexvol();

	/**
	 * Sets the value for measures/volumetric/rhCortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhcortexvol(Double v);

	/**
	 * @return Returns the measures/volumetric/CortexVol.
	 */
	public Double getMeasures_volumetric_cortexvol();

	/**
	 * Sets the value for measures/volumetric/CortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_cortexvol(Double v);

	/**
	 * @return Returns the measures/volumetric/SubCortGrayVol.
	 */
	public Double getMeasures_volumetric_subcortgrayvol();

	/**
	 * Sets the value for measures/volumetric/SubCortGrayVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_subcortgrayvol(Double v);

	/**
	 * @return Returns the measures/volumetric/TotalGrayVol.
	 */
	public Double getMeasures_volumetric_totalgrayvol();

	/**
	 * Sets the value for measures/volumetric/TotalGrayVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_totalgrayvol(Double v);

	/**
	 * @return Returns the measures/volumetric/SupraTentorialVol.
	 */
	public Double getMeasures_volumetric_supratentorialvol();

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvol(Double v);

	/**
	 * @return Returns the measures/volumetric/lhCorticalWhiteMatterVol.
	 */
	public Double getMeasures_volumetric_lhcorticalwhitemattervol();

	/**
	 * Sets the value for measures/volumetric/lhCorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhcorticalwhitemattervol(Double v);

	/**
	 * @return Returns the measures/volumetric/rhCorticalWhiteMatterVol.
	 */
	public Double getMeasures_volumetric_rhcorticalwhitemattervol();

	/**
	 * Sets the value for measures/volumetric/rhCorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhcorticalwhitemattervol(Double v);

	/**
	 * @return Returns the measures/volumetric/CorticalWhiteMatterVol.
	 */
	public Double getMeasures_volumetric_corticalwhitemattervol();

	/**
	 * Sets the value for measures/volumetric/CorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_corticalwhitemattervol(Double v);

	/**
	 * @return Returns the measures/volumetric/BrainSegVol.
	 */
	public Double getMeasures_volumetric_brainsegvol();

	/**
	 * Sets the value for measures/volumetric/BrainSegVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvol(Double v);

	/**
	 * @return Returns the measures/volumetric/BrainSegVolNotVent.
	 */
	public Double getMeasures_volumetric_brainsegvolnotvent();

	/**
	 * Sets the value for measures/volumetric/BrainSegVolNotVent.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolnotvent(Double v);

	/**
	 * @return Returns the measures/volumetric/BrainSegVolNotVentSurf.
	 */
	public Double getMeasures_volumetric_brainsegvolnotventsurf();

	/**
	 * Sets the value for measures/volumetric/BrainSegVolNotVentSurf.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolnotventsurf(Double v);

	/**
	 * @return Returns the measures/volumetric/SupraTentorialVolNotVent.
	 */
	public Double getMeasures_volumetric_supratentorialvolnotvent();

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVolNotVent.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvolnotvent(Double v);

	/**
	 * @return Returns the measures/volumetric/SupraTentorialVolNotVentVox.
	 */
	public Double getMeasures_volumetric_supratentorialvolnotventvox();

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVolNotVentVox.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvolnotventvox(Double v);

	/**
	 * @return Returns the measures/volumetric/MaskVol.
	 */
	public Double getMeasures_volumetric_maskvol();

	/**
	 * Sets the value for measures/volumetric/MaskVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_maskvol(Double v);

	/**
	 * @return Returns the measures/volumetric/BrainSegVol-to-eTIV.
	 */
	public Double getMeasures_volumetric_brainsegvolToEtiv();

	/**
	 * Sets the value for measures/volumetric/BrainSegVol-to-eTIV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolToEtiv(Double v);

	/**
	 * @return Returns the measures/volumetric/MaskVol-to-eTIV.
	 */
	public Double getMeasures_volumetric_maskvolToEtiv();

	/**
	 * Sets the value for measures/volumetric/MaskVol-to-eTIV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_maskvolToEtiv(Double v);

	/**
	 * @return Returns the measures/volumetric/lhSurfaceHoles.
	 */
	public Double getMeasures_volumetric_lhsurfaceholes();

	/**
	 * Sets the value for measures/volumetric/lhSurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhsurfaceholes(Double v);

	/**
	 * @return Returns the measures/volumetric/rhSurfaceHoles.
	 */
	public Double getMeasures_volumetric_rhsurfaceholes();

	/**
	 * Sets the value for measures/volumetric/rhSurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhsurfaceholes(Double v);

	/**
	 * @return Returns the measures/volumetric/SurfaceHoles.
	 */
	public Double getMeasures_volumetric_surfaceholes();

	/**
	 * Sets the value for measures/volumetric/SurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_surfaceholes(Double v);

	/**
	 * measures/volumetric/regions/region
	 * @return Returns an List of org.nrg.xdat.model.FsFsdataRegionI
	 */
	public <A extends org.nrg.xdat.model.FsFsdataRegionI> List<A> getMeasures_volumetric_regions_region();

	/**
	 * measures/volumetric/regions/region
	 * @return Returns an List of org.nrg.xdat.model.FsFsdataRegionI
	 */
	public <A extends org.nrg.xdat.model.FsFsdataRegionI> void addMeasures_volumetric_regions_region(A item) throws Exception;

	/**
	 * measures/surface/hemisphere
	 * @return Returns an List of org.nrg.xdat.model.FsFsdataHemisphereI
	 */
	public <A extends org.nrg.xdat.model.FsFsdataHemisphereI> List<A> getMeasures_surface_hemisphere();

	/**
	 * measures/surface/hemisphere
	 * @return Returns an List of org.nrg.xdat.model.FsFsdataHemisphereI
	 */
	public <A extends org.nrg.xdat.model.FsFsdataHemisphereI> void addMeasures_surface_hemisphere(A item) throws Exception;
}
