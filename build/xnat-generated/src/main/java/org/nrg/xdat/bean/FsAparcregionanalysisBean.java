/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:48 CST 2017
 *
 */
package org.nrg.xdat.bean;
import org.apache.log4j.Logger;
import org.nrg.xdat.bean.base.BaseElement;

import java.util.*;

/**
 * @author XDAT
 *
 *//*
 ******************************** 
 * DO NOT MODIFY THIS FILE 
 *
 ********************************/
@SuppressWarnings({"unchecked","rawtypes"})
public class FsAparcregionanalysisBean extends XnatMrassessordataBean implements java.io.Serializable, org.nrg.xdat.model.FsAparcregionanalysisI {
	public static final Logger logger = Logger.getLogger(FsAparcregionanalysisBean.class);
	public static final String SCHEMA_ELEMENT_NAME="fs:aparcRegionAnalysis";

	public String getSchemaElementName(){
		return "aparcRegionAnalysis";
	}

	public String getFullSchemaElementName(){
		return "fs:aparcRegionAnalysis";
	}
	 private List<org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean> _Hemisphere =new ArrayList<org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean>();

	/**
	 * hemisphere
	 * @return Returns an List of org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean
	 */
	public <A extends org.nrg.xdat.model.FsAparcregionanalysisHemisphereI> List<A> getHemisphere() {
		return (List<A>) _Hemisphere;
	}

	/**
	 * Sets the value for hemisphere.
	 * @param v Value to Set.
	 */
	public void setHemisphere(ArrayList<org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean> v){
		_Hemisphere=v;
	}

	/**
	 * Adds the value for hemisphere.
	 * @param v Value to Set.
	 */
	public void addHemisphere(org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean v){
		_Hemisphere.add(v);
	}

	/**
	 * hemisphere
	 * Adds org.nrg.xdat.model.FsAparcregionanalysisHemisphereI
	 */
	public <A extends org.nrg.xdat.model.FsAparcregionanalysisHemisphereI> void addHemisphere(A item) throws Exception{
	_Hemisphere.add((org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean)item);
	}

	/**
	 * Adds the value for hemisphere.
	 * @param v Value to Set.
	 */
	public void addHemisphere(Object v){
		if (v instanceof org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean)
			_Hemisphere.add((org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean)v);
		else
			throw new IllegalArgumentException("Must be a valid org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean");
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public void setDataField(String xmlPath,String v) throws BaseElement.UnknownFieldException{
			super.setDataField(xmlPath,v);
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public void setReferenceField(String xmlPath,BaseElement v) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("hemisphere")){
			addHemisphere(v);
		}
		else{
			super.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public Object getDataFieldValue(String xmlPath) throws BaseElement.UnknownFieldException{
			return super.getDataFieldValue(xmlPath);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public Object getReferenceField(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("hemisphere")){
			return getHemisphere();
		}
		else{
			return super.getReferenceField(xmlPath);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public String getReferenceFieldName(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("hemisphere")){
			return "http://nrg.wustl.edu/fs:aparcRegionAnalysis_hemisphere";
		}
		else{
			return super.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	public String getFieldType(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("hemisphere")){
			return BaseElement.field_multi_reference;
		}
		else{
			return super.getFieldType(xmlPath);
		}
	}

	/**
	 * Returns arraylist of all fields
	 */
	public ArrayList getAllFields() {
		ArrayList all_fields=new ArrayList();
		all_fields.add("hemisphere");
		all_fields.addAll(super.getAllFields());
		return all_fields;
	}


	public String toString(){
		java.io.StringWriter sw = new java.io.StringWriter();
		try{this.toXML(sw,true);}catch(java.io.IOException e){}
		return sw.toString();
	}


	public void toXML(java.io.Writer writer,boolean prettyPrint) throws java.io.IOException{
		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		writer.write("\n<fs:APARCRegionAnalysis");
		TreeMap map = new TreeMap();
		map.putAll(getXMLAtts());
		map.put("xmlns:arc","http://nrg.wustl.edu/arc");
		map.put("xmlns:cat","http://nrg.wustl.edu/catalog");
		map.put("xmlns:fs","http://nrg.wustl.edu/fs");
		map.put("xmlns:pipe","http://nrg.wustl.edu/pipe");
		map.put("xmlns:prov","http://www.nbirn.net/prov");
		map.put("xmlns:scr","http://nrg.wustl.edu/scr");
		map.put("xmlns:val","http://nrg.wustl.edu/val");
		map.put("xmlns:wrk","http://nrg.wustl.edu/workflow");
		map.put("xmlns:xdat","http://nrg.wustl.edu/security");
		map.put("xmlns:xnat","http://nrg.wustl.edu/xnat");
		map.put("xmlns:xnat_a","http://nrg.wustl.edu/xnat_assessments");
		map.put("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		java.util.Iterator iter =map.keySet().iterator();
		while(iter.hasNext()){
			String key = (String)iter.next();
			writer.write(" " + key + "=\"" + map.get(key) + "\"");
		}
		int header = 0;
		if (prettyPrint)header++;
		writer.write(">");
		addXMLBody(writer,header);
		if (prettyPrint)header--;
		writer.write("\n</fs:APARCRegionAnalysis>");
	}


	protected void addXMLAtts(java.io.Writer writer) throws java.io.IOException{
		TreeMap map = this.getXMLAtts();
		java.util.Iterator iter =map.keySet().iterator();
		while(iter.hasNext()){
			String key = (String)iter.next();
			writer.write(" " + key + "=\"" + map.get(key) + "\"");
		}
	}


	protected TreeMap getXMLAtts() {
		TreeMap map = super.getXMLAtts();
		return map;
	}


	protected boolean addXMLBody(java.io.Writer writer, int header) throws java.io.IOException{
		super.addXMLBody(writer,header);
		//REFERENCE FROM aparcRegionAnalysis -> mrAssessorData
		//REFERENCE FROM aparcRegionAnalysis -> hemisphere
		java.util.Iterator iter0=_Hemisphere.iterator();
		while(iter0.hasNext()){
			org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean child = (org.nrg.xdat.bean.FsAparcregionanalysisHemisphereBean)iter0.next();
			writer.write("\n" + createHeader(header++) + "<fs:hemisphere");
			child.addXMLAtts(writer);
			if(!child.getFullSchemaElementName().equals("fs:aparcRegionAnalysis_hemisphere")){
				writer.write(" xsi:type=\"" + child.getFullSchemaElementName() + "\"");
			}
			if (child.hasXMLBodyContent()){
				writer.write(">");
				boolean return1 =child.addXMLBody(writer,header);
				if(return1){
					writer.write("\n" + createHeader(--header) + "</fs:hemisphere>");
				}else{
					writer.write("</fs:hemisphere>");
					header--;
				}
			}else {writer.write("/>");header--;}
		}
	return true;
	}


	protected boolean hasXMLBodyContent(){
		if(_Hemisphere.size()>0) return true;
		if(super.hasXMLBodyContent())return true;
		return false;
	}
}
