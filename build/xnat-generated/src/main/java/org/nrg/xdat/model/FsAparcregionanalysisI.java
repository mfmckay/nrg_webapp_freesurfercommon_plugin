/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:48 CST 2017
 *
 */
package org.nrg.xdat.model;

import java.util.List;

/**
 * @author XDAT
 *
 */
public interface FsAparcregionanalysisI extends XnatMrassessordataI {

	public String getXSIType();

	public void toXML(java.io.Writer writer) throws java.lang.Exception;

	/**
	 * hemisphere
	 * @return Returns an List of org.nrg.xdat.model.FsAparcregionanalysisHemisphereI
	 */
	public <A extends org.nrg.xdat.model.FsAparcregionanalysisHemisphereI> List<A> getHemisphere();

	/**
	 * hemisphere
	 * @return Returns an List of org.nrg.xdat.model.FsAparcregionanalysisHemisphereI
	 */
	public <A extends org.nrg.xdat.model.FsAparcregionanalysisHemisphereI> void addHemisphere(A item) throws Exception;
}
