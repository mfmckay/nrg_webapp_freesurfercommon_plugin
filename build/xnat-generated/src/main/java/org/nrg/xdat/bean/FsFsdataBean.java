/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:48 CST 2017
 *
 */
package org.nrg.xdat.bean;
import org.apache.log4j.Logger;
import org.nrg.xdat.bean.base.BaseElement;

import java.util.*;

/**
 * @author XDAT
 *
 *//*
 ******************************** 
 * DO NOT MODIFY THIS FILE 
 *
 ********************************/
@SuppressWarnings({"unchecked","rawtypes"})
public class FsFsdataBean extends XnatImageassessordataBean implements java.io.Serializable, org.nrg.xdat.model.FsFsdataI {
	public static final Logger logger = Logger.getLogger(FsFsdataBean.class);
	public static final String SCHEMA_ELEMENT_NAME="fs:fsData";

	public String getSchemaElementName(){
		return "fsData";
	}

	public String getFullSchemaElementName(){
		return "fs:fsData";
	}

	//FIELD

	private String _FsVersion=null;

	/**
	 * @return Returns the fs_version.
	 */
	public String getFsVersion(){
		return _FsVersion;
	}

	/**
	 * Sets the value for fs_version.
	 * @param v Value to Set.
	 */
	public void setFsVersion(String v){
		_FsVersion=v;
	}

	//FIELD

	private Double _IlpAge=null;

	/**
	 * @return Returns the ilp_age.
	 */
	public Double getIlpAge() {
		return _IlpAge;
	}

	/**
	 * Sets the value for ilp_age.
	 * @param v Value to Set.
	 */
	public void setIlpAge(Double v){
		_IlpAge=v;
	}

	/**
	 * Sets the value for ilp_age.
	 * @param v Value to Set.
	 */
	public void setIlpAge(String v)  {
		_IlpAge=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_icv=null;

	/**
	 * @return Returns the measures/volumetric/ICV.
	 */
	public Double getMeasures_volumetric_icv() {
		return _Measures_volumetric_icv;
	}

	/**
	 * Sets the value for measures/volumetric/ICV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_icv(Double v){
		_Measures_volumetric_icv=v;
	}

	/**
	 * Sets the value for measures/volumetric/ICV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_icv(String v)  {
		_Measures_volumetric_icv=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_lhcortexvol=null;

	/**
	 * @return Returns the measures/volumetric/lhCortexVol.
	 */
	public Double getMeasures_volumetric_lhcortexvol() {
		return _Measures_volumetric_lhcortexvol;
	}

	/**
	 * Sets the value for measures/volumetric/lhCortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhcortexvol(Double v){
		_Measures_volumetric_lhcortexvol=v;
	}

	/**
	 * Sets the value for measures/volumetric/lhCortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhcortexvol(String v)  {
		_Measures_volumetric_lhcortexvol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_rhcortexvol=null;

	/**
	 * @return Returns the measures/volumetric/rhCortexVol.
	 */
	public Double getMeasures_volumetric_rhcortexvol() {
		return _Measures_volumetric_rhcortexvol;
	}

	/**
	 * Sets the value for measures/volumetric/rhCortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhcortexvol(Double v){
		_Measures_volumetric_rhcortexvol=v;
	}

	/**
	 * Sets the value for measures/volumetric/rhCortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhcortexvol(String v)  {
		_Measures_volumetric_rhcortexvol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_cortexvol=null;

	/**
	 * @return Returns the measures/volumetric/CortexVol.
	 */
	public Double getMeasures_volumetric_cortexvol() {
		return _Measures_volumetric_cortexvol;
	}

	/**
	 * Sets the value for measures/volumetric/CortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_cortexvol(Double v){
		_Measures_volumetric_cortexvol=v;
	}

	/**
	 * Sets the value for measures/volumetric/CortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_cortexvol(String v)  {
		_Measures_volumetric_cortexvol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_subcortgrayvol=null;

	/**
	 * @return Returns the measures/volumetric/SubCortGrayVol.
	 */
	public Double getMeasures_volumetric_subcortgrayvol() {
		return _Measures_volumetric_subcortgrayvol;
	}

	/**
	 * Sets the value for measures/volumetric/SubCortGrayVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_subcortgrayvol(Double v){
		_Measures_volumetric_subcortgrayvol=v;
	}

	/**
	 * Sets the value for measures/volumetric/SubCortGrayVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_subcortgrayvol(String v)  {
		_Measures_volumetric_subcortgrayvol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_totalgrayvol=null;

	/**
	 * @return Returns the measures/volumetric/TotalGrayVol.
	 */
	public Double getMeasures_volumetric_totalgrayvol() {
		return _Measures_volumetric_totalgrayvol;
	}

	/**
	 * Sets the value for measures/volumetric/TotalGrayVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_totalgrayvol(Double v){
		_Measures_volumetric_totalgrayvol=v;
	}

	/**
	 * Sets the value for measures/volumetric/TotalGrayVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_totalgrayvol(String v)  {
		_Measures_volumetric_totalgrayvol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_supratentorialvol=null;

	/**
	 * @return Returns the measures/volumetric/SupraTentorialVol.
	 */
	public Double getMeasures_volumetric_supratentorialvol() {
		return _Measures_volumetric_supratentorialvol;
	}

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvol(Double v){
		_Measures_volumetric_supratentorialvol=v;
	}

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvol(String v)  {
		_Measures_volumetric_supratentorialvol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_lhcorticalwhitemattervol=null;

	/**
	 * @return Returns the measures/volumetric/lhCorticalWhiteMatterVol.
	 */
	public Double getMeasures_volumetric_lhcorticalwhitemattervol() {
		return _Measures_volumetric_lhcorticalwhitemattervol;
	}

	/**
	 * Sets the value for measures/volumetric/lhCorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhcorticalwhitemattervol(Double v){
		_Measures_volumetric_lhcorticalwhitemattervol=v;
	}

	/**
	 * Sets the value for measures/volumetric/lhCorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhcorticalwhitemattervol(String v)  {
		_Measures_volumetric_lhcorticalwhitemattervol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_rhcorticalwhitemattervol=null;

	/**
	 * @return Returns the measures/volumetric/rhCorticalWhiteMatterVol.
	 */
	public Double getMeasures_volumetric_rhcorticalwhitemattervol() {
		return _Measures_volumetric_rhcorticalwhitemattervol;
	}

	/**
	 * Sets the value for measures/volumetric/rhCorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhcorticalwhitemattervol(Double v){
		_Measures_volumetric_rhcorticalwhitemattervol=v;
	}

	/**
	 * Sets the value for measures/volumetric/rhCorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhcorticalwhitemattervol(String v)  {
		_Measures_volumetric_rhcorticalwhitemattervol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_corticalwhitemattervol=null;

	/**
	 * @return Returns the measures/volumetric/CorticalWhiteMatterVol.
	 */
	public Double getMeasures_volumetric_corticalwhitemattervol() {
		return _Measures_volumetric_corticalwhitemattervol;
	}

	/**
	 * Sets the value for measures/volumetric/CorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_corticalwhitemattervol(Double v){
		_Measures_volumetric_corticalwhitemattervol=v;
	}

	/**
	 * Sets the value for measures/volumetric/CorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_corticalwhitemattervol(String v)  {
		_Measures_volumetric_corticalwhitemattervol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_brainsegvol=null;

	/**
	 * @return Returns the measures/volumetric/BrainSegVol.
	 */
	public Double getMeasures_volumetric_brainsegvol() {
		return _Measures_volumetric_brainsegvol;
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvol(Double v){
		_Measures_volumetric_brainsegvol=v;
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvol(String v)  {
		_Measures_volumetric_brainsegvol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_brainsegvolnotvent=null;

	/**
	 * @return Returns the measures/volumetric/BrainSegVolNotVent.
	 */
	public Double getMeasures_volumetric_brainsegvolnotvent() {
		return _Measures_volumetric_brainsegvolnotvent;
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVolNotVent.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolnotvent(Double v){
		_Measures_volumetric_brainsegvolnotvent=v;
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVolNotVent.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolnotvent(String v)  {
		_Measures_volumetric_brainsegvolnotvent=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_brainsegvolnotventsurf=null;

	/**
	 * @return Returns the measures/volumetric/BrainSegVolNotVentSurf.
	 */
	public Double getMeasures_volumetric_brainsegvolnotventsurf() {
		return _Measures_volumetric_brainsegvolnotventsurf;
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVolNotVentSurf.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolnotventsurf(Double v){
		_Measures_volumetric_brainsegvolnotventsurf=v;
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVolNotVentSurf.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolnotventsurf(String v)  {
		_Measures_volumetric_brainsegvolnotventsurf=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_supratentorialvolnotvent=null;

	/**
	 * @return Returns the measures/volumetric/SupraTentorialVolNotVent.
	 */
	public Double getMeasures_volumetric_supratentorialvolnotvent() {
		return _Measures_volumetric_supratentorialvolnotvent;
	}

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVolNotVent.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvolnotvent(Double v){
		_Measures_volumetric_supratentorialvolnotvent=v;
	}

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVolNotVent.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvolnotvent(String v)  {
		_Measures_volumetric_supratentorialvolnotvent=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_supratentorialvolnotventvox=null;

	/**
	 * @return Returns the measures/volumetric/SupraTentorialVolNotVentVox.
	 */
	public Double getMeasures_volumetric_supratentorialvolnotventvox() {
		return _Measures_volumetric_supratentorialvolnotventvox;
	}

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVolNotVentVox.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvolnotventvox(Double v){
		_Measures_volumetric_supratentorialvolnotventvox=v;
	}

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVolNotVentVox.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvolnotventvox(String v)  {
		_Measures_volumetric_supratentorialvolnotventvox=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_maskvol=null;

	/**
	 * @return Returns the measures/volumetric/MaskVol.
	 */
	public Double getMeasures_volumetric_maskvol() {
		return _Measures_volumetric_maskvol;
	}

	/**
	 * Sets the value for measures/volumetric/MaskVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_maskvol(Double v){
		_Measures_volumetric_maskvol=v;
	}

	/**
	 * Sets the value for measures/volumetric/MaskVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_maskvol(String v)  {
		_Measures_volumetric_maskvol=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_brainsegvolToEtiv=null;

	/**
	 * @return Returns the measures/volumetric/BrainSegVol-to-eTIV.
	 */
	public Double getMeasures_volumetric_brainsegvolToEtiv() {
		return _Measures_volumetric_brainsegvolToEtiv;
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVol-to-eTIV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolToEtiv(Double v){
		_Measures_volumetric_brainsegvolToEtiv=v;
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVol-to-eTIV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolToEtiv(String v)  {
		_Measures_volumetric_brainsegvolToEtiv=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_maskvolToEtiv=null;

	/**
	 * @return Returns the measures/volumetric/MaskVol-to-eTIV.
	 */
	public Double getMeasures_volumetric_maskvolToEtiv() {
		return _Measures_volumetric_maskvolToEtiv;
	}

	/**
	 * Sets the value for measures/volumetric/MaskVol-to-eTIV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_maskvolToEtiv(Double v){
		_Measures_volumetric_maskvolToEtiv=v;
	}

	/**
	 * Sets the value for measures/volumetric/MaskVol-to-eTIV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_maskvolToEtiv(String v)  {
		_Measures_volumetric_maskvolToEtiv=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_lhsurfaceholes=null;

	/**
	 * @return Returns the measures/volumetric/lhSurfaceHoles.
	 */
	public Double getMeasures_volumetric_lhsurfaceholes() {
		return _Measures_volumetric_lhsurfaceholes;
	}

	/**
	 * Sets the value for measures/volumetric/lhSurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhsurfaceholes(Double v){
		_Measures_volumetric_lhsurfaceholes=v;
	}

	/**
	 * Sets the value for measures/volumetric/lhSurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhsurfaceholes(String v)  {
		_Measures_volumetric_lhsurfaceholes=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_rhsurfaceholes=null;

	/**
	 * @return Returns the measures/volumetric/rhSurfaceHoles.
	 */
	public Double getMeasures_volumetric_rhsurfaceholes() {
		return _Measures_volumetric_rhsurfaceholes;
	}

	/**
	 * Sets the value for measures/volumetric/rhSurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhsurfaceholes(Double v){
		_Measures_volumetric_rhsurfaceholes=v;
	}

	/**
	 * Sets the value for measures/volumetric/rhSurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhsurfaceholes(String v)  {
		_Measures_volumetric_rhsurfaceholes=formatDouble(v);
	}

	//FIELD

	private Double _Measures_volumetric_surfaceholes=null;

	/**
	 * @return Returns the measures/volumetric/SurfaceHoles.
	 */
	public Double getMeasures_volumetric_surfaceholes() {
		return _Measures_volumetric_surfaceholes;
	}

	/**
	 * Sets the value for measures/volumetric/SurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_surfaceholes(Double v){
		_Measures_volumetric_surfaceholes=v;
	}

	/**
	 * Sets the value for measures/volumetric/SurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_surfaceholes(String v)  {
		_Measures_volumetric_surfaceholes=formatDouble(v);
	}
	 private List<org.nrg.xdat.bean.FsFsdataRegionBean> _Measures_volumetric_regions_region =new ArrayList<org.nrg.xdat.bean.FsFsdataRegionBean>();

	/**
	 * measures/volumetric/regions/region
	 * @return Returns an List of org.nrg.xdat.bean.FsFsdataRegionBean
	 */
	public <A extends org.nrg.xdat.model.FsFsdataRegionI> List<A> getMeasures_volumetric_regions_region() {
		return (List<A>) _Measures_volumetric_regions_region;
	}

	/**
	 * Sets the value for measures/volumetric/regions/region.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_regions_region(ArrayList<org.nrg.xdat.bean.FsFsdataRegionBean> v){
		_Measures_volumetric_regions_region=v;
	}

	/**
	 * Adds the value for measures/volumetric/regions/region.
	 * @param v Value to Set.
	 */
	public void addMeasures_volumetric_regions_region(org.nrg.xdat.bean.FsFsdataRegionBean v){
		_Measures_volumetric_regions_region.add(v);
	}

	/**
	 * measures/volumetric/regions/region
	 * Adds org.nrg.xdat.model.FsFsdataRegionI
	 */
	public <A extends org.nrg.xdat.model.FsFsdataRegionI> void addMeasures_volumetric_regions_region(A item) throws Exception{
	_Measures_volumetric_regions_region.add((org.nrg.xdat.bean.FsFsdataRegionBean)item);
	}

	/**
	 * Adds the value for measures/volumetric/regions/region.
	 * @param v Value to Set.
	 */
	public void addMeasures_volumetric_regions_region(Object v){
		if (v instanceof org.nrg.xdat.bean.FsFsdataRegionBean)
			_Measures_volumetric_regions_region.add((org.nrg.xdat.bean.FsFsdataRegionBean)v);
		else
			throw new IllegalArgumentException("Must be a valid org.nrg.xdat.bean.FsFsdataRegionBean");
	}
	 private List<org.nrg.xdat.bean.FsFsdataHemisphereBean> _Measures_surface_hemisphere =new ArrayList<org.nrg.xdat.bean.FsFsdataHemisphereBean>();

	/**
	 * measures/surface/hemisphere
	 * @return Returns an List of org.nrg.xdat.bean.FsFsdataHemisphereBean
	 */
	public <A extends org.nrg.xdat.model.FsFsdataHemisphereI> List<A> getMeasures_surface_hemisphere() {
		return (List<A>) _Measures_surface_hemisphere;
	}

	/**
	 * Sets the value for measures/surface/hemisphere.
	 * @param v Value to Set.
	 */
	public void setMeasures_surface_hemisphere(ArrayList<org.nrg.xdat.bean.FsFsdataHemisphereBean> v){
		_Measures_surface_hemisphere=v;
	}

	/**
	 * Adds the value for measures/surface/hemisphere.
	 * @param v Value to Set.
	 */
	public void addMeasures_surface_hemisphere(org.nrg.xdat.bean.FsFsdataHemisphereBean v){
		_Measures_surface_hemisphere.add(v);
	}

	/**
	 * measures/surface/hemisphere
	 * Adds org.nrg.xdat.model.FsFsdataHemisphereI
	 */
	public <A extends org.nrg.xdat.model.FsFsdataHemisphereI> void addMeasures_surface_hemisphere(A item) throws Exception{
	_Measures_surface_hemisphere.add((org.nrg.xdat.bean.FsFsdataHemisphereBean)item);
	}

	/**
	 * Adds the value for measures/surface/hemisphere.
	 * @param v Value to Set.
	 */
	public void addMeasures_surface_hemisphere(Object v){
		if (v instanceof org.nrg.xdat.bean.FsFsdataHemisphereBean)
			_Measures_surface_hemisphere.add((org.nrg.xdat.bean.FsFsdataHemisphereBean)v);
		else
			throw new IllegalArgumentException("Must be a valid org.nrg.xdat.bean.FsFsdataHemisphereBean");
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public void setDataField(String xmlPath,String v) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("fs_version")){
			setFsVersion(v);
		}else if (xmlPath.equals("ilp_age")){
			setIlpAge(v);
		}else if (xmlPath.equals("measures/volumetric/ICV")){
			setMeasures_volumetric_icv(v);
		}else if (xmlPath.equals("measures/volumetric/lhCortexVol")){
			setMeasures_volumetric_lhcortexvol(v);
		}else if (xmlPath.equals("measures/volumetric/rhCortexVol")){
			setMeasures_volumetric_rhcortexvol(v);
		}else if (xmlPath.equals("measures/volumetric/CortexVol")){
			setMeasures_volumetric_cortexvol(v);
		}else if (xmlPath.equals("measures/volumetric/SubCortGrayVol")){
			setMeasures_volumetric_subcortgrayvol(v);
		}else if (xmlPath.equals("measures/volumetric/TotalGrayVol")){
			setMeasures_volumetric_totalgrayvol(v);
		}else if (xmlPath.equals("measures/volumetric/SupraTentorialVol")){
			setMeasures_volumetric_supratentorialvol(v);
		}else if (xmlPath.equals("measures/volumetric/lhCorticalWhiteMatterVol")){
			setMeasures_volumetric_lhcorticalwhitemattervol(v);
		}else if (xmlPath.equals("measures/volumetric/rhCorticalWhiteMatterVol")){
			setMeasures_volumetric_rhcorticalwhitemattervol(v);
		}else if (xmlPath.equals("measures/volumetric/CorticalWhiteMatterVol")){
			setMeasures_volumetric_corticalwhitemattervol(v);
		}else if (xmlPath.equals("measures/volumetric/BrainSegVol")){
			setMeasures_volumetric_brainsegvol(v);
		}else if (xmlPath.equals("measures/volumetric/BrainSegVolNotVent")){
			setMeasures_volumetric_brainsegvolnotvent(v);
		}else if (xmlPath.equals("measures/volumetric/BrainSegVolNotVentSurf")){
			setMeasures_volumetric_brainsegvolnotventsurf(v);
		}else if (xmlPath.equals("measures/volumetric/SupraTentorialVolNotVent")){
			setMeasures_volumetric_supratentorialvolnotvent(v);
		}else if (xmlPath.equals("measures/volumetric/SupraTentorialVolNotVentVox")){
			setMeasures_volumetric_supratentorialvolnotventvox(v);
		}else if (xmlPath.equals("measures/volumetric/MaskVol")){
			setMeasures_volumetric_maskvol(v);
		}else if (xmlPath.equals("measures/volumetric/BrainSegVol-to-eTIV")){
			setMeasures_volumetric_brainsegvolToEtiv(v);
		}else if (xmlPath.equals("measures/volumetric/MaskVol-to-eTIV")){
			setMeasures_volumetric_maskvolToEtiv(v);
		}else if (xmlPath.equals("measures/volumetric/lhSurfaceHoles")){
			setMeasures_volumetric_lhsurfaceholes(v);
		}else if (xmlPath.equals("measures/volumetric/rhSurfaceHoles")){
			setMeasures_volumetric_rhsurfaceholes(v);
		}else if (xmlPath.equals("measures/volumetric/SurfaceHoles")){
			setMeasures_volumetric_surfaceholes(v);
		}
		else{
			super.setDataField(xmlPath,v);
		}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public void setReferenceField(String xmlPath,BaseElement v) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("measures/volumetric/regions/region")){
			addMeasures_volumetric_regions_region(v);
		}else if (xmlPath.equals("measures/surface/hemisphere")){
			addMeasures_surface_hemisphere(v);
		}
		else{
			super.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public Object getDataFieldValue(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("fs_version")){
			return getFsVersion();
		}else if (xmlPath.equals("ilp_age")){
			return getIlpAge();
		}else if (xmlPath.equals("measures/volumetric/ICV")){
			return getMeasures_volumetric_icv();
		}else if (xmlPath.equals("measures/volumetric/lhCortexVol")){
			return getMeasures_volumetric_lhcortexvol();
		}else if (xmlPath.equals("measures/volumetric/rhCortexVol")){
			return getMeasures_volumetric_rhcortexvol();
		}else if (xmlPath.equals("measures/volumetric/CortexVol")){
			return getMeasures_volumetric_cortexvol();
		}else if (xmlPath.equals("measures/volumetric/SubCortGrayVol")){
			return getMeasures_volumetric_subcortgrayvol();
		}else if (xmlPath.equals("measures/volumetric/TotalGrayVol")){
			return getMeasures_volumetric_totalgrayvol();
		}else if (xmlPath.equals("measures/volumetric/SupraTentorialVol")){
			return getMeasures_volumetric_supratentorialvol();
		}else if (xmlPath.equals("measures/volumetric/lhCorticalWhiteMatterVol")){
			return getMeasures_volumetric_lhcorticalwhitemattervol();
		}else if (xmlPath.equals("measures/volumetric/rhCorticalWhiteMatterVol")){
			return getMeasures_volumetric_rhcorticalwhitemattervol();
		}else if (xmlPath.equals("measures/volumetric/CorticalWhiteMatterVol")){
			return getMeasures_volumetric_corticalwhitemattervol();
		}else if (xmlPath.equals("measures/volumetric/BrainSegVol")){
			return getMeasures_volumetric_brainsegvol();
		}else if (xmlPath.equals("measures/volumetric/BrainSegVolNotVent")){
			return getMeasures_volumetric_brainsegvolnotvent();
		}else if (xmlPath.equals("measures/volumetric/BrainSegVolNotVentSurf")){
			return getMeasures_volumetric_brainsegvolnotventsurf();
		}else if (xmlPath.equals("measures/volumetric/SupraTentorialVolNotVent")){
			return getMeasures_volumetric_supratentorialvolnotvent();
		}else if (xmlPath.equals("measures/volumetric/SupraTentorialVolNotVentVox")){
			return getMeasures_volumetric_supratentorialvolnotventvox();
		}else if (xmlPath.equals("measures/volumetric/MaskVol")){
			return getMeasures_volumetric_maskvol();
		}else if (xmlPath.equals("measures/volumetric/BrainSegVol-to-eTIV")){
			return getMeasures_volumetric_brainsegvolToEtiv();
		}else if (xmlPath.equals("measures/volumetric/MaskVol-to-eTIV")){
			return getMeasures_volumetric_maskvolToEtiv();
		}else if (xmlPath.equals("measures/volumetric/lhSurfaceHoles")){
			return getMeasures_volumetric_lhsurfaceholes();
		}else if (xmlPath.equals("measures/volumetric/rhSurfaceHoles")){
			return getMeasures_volumetric_rhsurfaceholes();
		}else if (xmlPath.equals("measures/volumetric/SurfaceHoles")){
			return getMeasures_volumetric_surfaceholes();
		}
		else{
			return super.getDataFieldValue(xmlPath);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public Object getReferenceField(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("measures/volumetric/regions/region")){
			return getMeasures_volumetric_regions_region();
		}else if (xmlPath.equals("measures/surface/hemisphere")){
			return getMeasures_surface_hemisphere();
		}
		else{
			return super.getReferenceField(xmlPath);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public String getReferenceFieldName(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("measures/volumetric/regions/region")){
			return "http://nrg.wustl.edu/fs:fsData_region";
		}else if (xmlPath.equals("measures/surface/hemisphere")){
			return "http://nrg.wustl.edu/fs:fsData_hemisphere";
		}
		else{
			return super.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	public String getFieldType(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("fs_version")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("ilp_age")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/ICV")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/lhCortexVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/rhCortexVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/CortexVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/SubCortGrayVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/TotalGrayVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/SupraTentorialVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/lhCorticalWhiteMatterVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/rhCorticalWhiteMatterVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/CorticalWhiteMatterVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/BrainSegVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/BrainSegVolNotVent")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/BrainSegVolNotVentSurf")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/SupraTentorialVolNotVent")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/SupraTentorialVolNotVentVox")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/MaskVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/BrainSegVol-to-eTIV")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/MaskVol-to-eTIV")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/lhSurfaceHoles")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/rhSurfaceHoles")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/SurfaceHoles")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("measures/volumetric/regions/region")){
			return BaseElement.field_multi_reference;
		}else if (xmlPath.equals("measures/surface/hemisphere")){
			return BaseElement.field_multi_reference;
		}
		else{
			return super.getFieldType(xmlPath);
		}
	}

	/**
	 * Returns arraylist of all fields
	 */
	public ArrayList getAllFields() {
		ArrayList all_fields=new ArrayList();
		all_fields.add("fs_version");
		all_fields.add("ilp_age");
		all_fields.add("measures/volumetric/ICV");
		all_fields.add("measures/volumetric/lhCortexVol");
		all_fields.add("measures/volumetric/rhCortexVol");
		all_fields.add("measures/volumetric/CortexVol");
		all_fields.add("measures/volumetric/SubCortGrayVol");
		all_fields.add("measures/volumetric/TotalGrayVol");
		all_fields.add("measures/volumetric/SupraTentorialVol");
		all_fields.add("measures/volumetric/lhCorticalWhiteMatterVol");
		all_fields.add("measures/volumetric/rhCorticalWhiteMatterVol");
		all_fields.add("measures/volumetric/CorticalWhiteMatterVol");
		all_fields.add("measures/volumetric/BrainSegVol");
		all_fields.add("measures/volumetric/BrainSegVolNotVent");
		all_fields.add("measures/volumetric/BrainSegVolNotVentSurf");
		all_fields.add("measures/volumetric/SupraTentorialVolNotVent");
		all_fields.add("measures/volumetric/SupraTentorialVolNotVentVox");
		all_fields.add("measures/volumetric/MaskVol");
		all_fields.add("measures/volumetric/BrainSegVol-to-eTIV");
		all_fields.add("measures/volumetric/MaskVol-to-eTIV");
		all_fields.add("measures/volumetric/lhSurfaceHoles");
		all_fields.add("measures/volumetric/rhSurfaceHoles");
		all_fields.add("measures/volumetric/SurfaceHoles");
		all_fields.add("measures/volumetric/regions/region");
		all_fields.add("measures/surface/hemisphere");
		all_fields.addAll(super.getAllFields());
		return all_fields;
	}


	public String toString(){
		java.io.StringWriter sw = new java.io.StringWriter();
		try{this.toXML(sw,true);}catch(java.io.IOException e){}
		return sw.toString();
	}


	public void toXML(java.io.Writer writer,boolean prettyPrint) throws java.io.IOException{
		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		writer.write("\n<fs:Freesurfer");
		TreeMap map = new TreeMap();
		map.putAll(getXMLAtts());
		map.put("xmlns:arc","http://nrg.wustl.edu/arc");
		map.put("xmlns:cat","http://nrg.wustl.edu/catalog");
		map.put("xmlns:fs","http://nrg.wustl.edu/fs");
		map.put("xmlns:pipe","http://nrg.wustl.edu/pipe");
		map.put("xmlns:prov","http://www.nbirn.net/prov");
		map.put("xmlns:scr","http://nrg.wustl.edu/scr");
		map.put("xmlns:val","http://nrg.wustl.edu/val");
		map.put("xmlns:wrk","http://nrg.wustl.edu/workflow");
		map.put("xmlns:xdat","http://nrg.wustl.edu/security");
		map.put("xmlns:xnat","http://nrg.wustl.edu/xnat");
		map.put("xmlns:xnat_a","http://nrg.wustl.edu/xnat_assessments");
		map.put("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		java.util.Iterator iter =map.keySet().iterator();
		while(iter.hasNext()){
			String key = (String)iter.next();
			writer.write(" " + key + "=\"" + map.get(key) + "\"");
		}
		int header = 0;
		if (prettyPrint)header++;
		writer.write(">");
		addXMLBody(writer,header);
		if (prettyPrint)header--;
		writer.write("\n</fs:Freesurfer>");
	}


	protected void addXMLAtts(java.io.Writer writer) throws java.io.IOException{
		TreeMap map = this.getXMLAtts();
		java.util.Iterator iter =map.keySet().iterator();
		while(iter.hasNext()){
			String key = (String)iter.next();
			writer.write(" " + key + "=\"" + map.get(key) + "\"");
		}
	}


	protected TreeMap getXMLAtts() {
		TreeMap map = super.getXMLAtts();
		return map;
	}


	protected boolean addXMLBody(java.io.Writer writer, int header) throws java.io.IOException{
		super.addXMLBody(writer,header);
		//REFERENCE FROM fsData -> imageAssessorData
		if (_FsVersion!=null){
			writer.write("\n" + createHeader(header++) + "<fs:fs_version");
			writer.write(">");
			writer.write(ValueParser(_FsVersion,"string"));
			writer.write("</fs:fs_version>");
			header--;
		}
		if (_IlpAge!=null){
			writer.write("\n" + createHeader(header++) + "<fs:ilp_age");
			writer.write(">");
			writer.write(ValueParser(_IlpAge,"float"));
			writer.write("</fs:ilp_age>");
			header--;
		}
			int child0=0;
			int att0=0;
			if(_Measures_volumetric_icv!=null)
			child0++;
			if(_Measures_volumetric_rhcortexvol!=null)
			child0++;
			if(_Measures_volumetric_lhcorticalwhitemattervol!=null)
			child0++;
			if(_Measures_volumetric_supratentorialvolnotventvox!=null)
			child0++;
			if(_Measures_volumetric_supratentorialvolnotvent!=null)
			child0++;
			if(_Measures_volumetric_surfaceholes!=null)
			child0++;
			if(_Measures_volumetric_subcortgrayvol!=null)
			child0++;
			if(_Measures_volumetric_brainsegvolnotventsurf!=null)
			child0++;
			if(_Measures_volumetric_corticalwhitemattervol!=null)
			child0++;
			if(_Measures_volumetric_brainsegvolnotvent!=null)
			child0++;
			if(_Measures_volumetric_lhsurfaceholes!=null)
			child0++;
			if(_Measures_volumetric_lhcortexvol!=null)
			child0++;
			if(_Measures_volumetric_rhsurfaceholes!=null)
			child0++;
			child0+=_Measures_surface_hemisphere.size();
			child0+=_Measures_volumetric_regions_region.size();
			if(_Measures_volumetric_cortexvol!=null)
			child0++;
			if(_Measures_volumetric_maskvolToEtiv!=null)
			child0++;
			if(_Measures_volumetric_rhcorticalwhitemattervol!=null)
			child0++;
			if(_Measures_volumetric_maskvol!=null)
			child0++;
			if(_Measures_volumetric_brainsegvol!=null)
			child0++;
			if(_Measures_volumetric_totalgrayvol!=null)
			child0++;
			if(_Measures_volumetric_brainsegvolToEtiv!=null)
			child0++;
			if(_Measures_volumetric_supratentorialvol!=null)
			child0++;
			if(child0>0 || att0>0){
				writer.write("\n" + createHeader(header++) + "<fs:measures");
			if(child0==0){
				writer.write("/>");
			}else{
				writer.write(">");
			int child1=0;
			int att1=0;
			if(_Measures_volumetric_icv!=null)
			child1++;
			if(_Measures_volumetric_rhcortexvol!=null)
			child1++;
			if(_Measures_volumetric_lhcorticalwhitemattervol!=null)
			child1++;
			if(_Measures_volumetric_supratentorialvolnotventvox!=null)
			child1++;
			if(_Measures_volumetric_supratentorialvolnotvent!=null)
			child1++;
			if(_Measures_volumetric_surfaceholes!=null)
			child1++;
			if(_Measures_volumetric_subcortgrayvol!=null)
			child1++;
			if(_Measures_volumetric_brainsegvolnotventsurf!=null)
			child1++;
			if(_Measures_volumetric_corticalwhitemattervol!=null)
			child1++;
			if(_Measures_volumetric_brainsegvolnotvent!=null)
			child1++;
			if(_Measures_volumetric_lhsurfaceholes!=null)
			child1++;
			if(_Measures_volumetric_lhcortexvol!=null)
			child1++;
			if(_Measures_volumetric_rhsurfaceholes!=null)
			child1++;
			child1+=_Measures_volumetric_regions_region.size();
			if(_Measures_volumetric_cortexvol!=null)
			child1++;
			if(_Measures_volumetric_maskvolToEtiv!=null)
			child1++;
			if(_Measures_volumetric_rhcorticalwhitemattervol!=null)
			child1++;
			if(_Measures_volumetric_maskvol!=null)
			child1++;
			if(_Measures_volumetric_brainsegvol!=null)
			child1++;
			if(_Measures_volumetric_totalgrayvol!=null)
			child1++;
			if(_Measures_volumetric_brainsegvolToEtiv!=null)
			child1++;
			if(_Measures_volumetric_supratentorialvol!=null)
			child1++;
			if(child1>0 || att1>0){
				writer.write("\n" + createHeader(header++) + "<fs:volumetric");
			if(child1==0){
				writer.write("/>");
			}else{
				writer.write(">");
		if (_Measures_volumetric_icv!=null){
			writer.write("\n" + createHeader(header++) + "<fs:ICV");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_icv,"float"));
			writer.write("</fs:ICV>");
			header--;
		}
		if (_Measures_volumetric_lhcortexvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:lhCortexVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_lhcortexvol,"float"));
			writer.write("</fs:lhCortexVol>");
			header--;
		}
		if (_Measures_volumetric_rhcortexvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:rhCortexVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_rhcortexvol,"float"));
			writer.write("</fs:rhCortexVol>");
			header--;
		}
		if (_Measures_volumetric_cortexvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:CortexVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_cortexvol,"float"));
			writer.write("</fs:CortexVol>");
			header--;
		}
		if (_Measures_volumetric_subcortgrayvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:SubCortGrayVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_subcortgrayvol,"float"));
			writer.write("</fs:SubCortGrayVol>");
			header--;
		}
		if (_Measures_volumetric_totalgrayvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:TotalGrayVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_totalgrayvol,"float"));
			writer.write("</fs:TotalGrayVol>");
			header--;
		}
		if (_Measures_volumetric_supratentorialvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:SupraTentorialVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_supratentorialvol,"float"));
			writer.write("</fs:SupraTentorialVol>");
			header--;
		}
		if (_Measures_volumetric_lhcorticalwhitemattervol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:lhCorticalWhiteMatterVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_lhcorticalwhitemattervol,"float"));
			writer.write("</fs:lhCorticalWhiteMatterVol>");
			header--;
		}
		if (_Measures_volumetric_rhcorticalwhitemattervol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:rhCorticalWhiteMatterVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_rhcorticalwhitemattervol,"float"));
			writer.write("</fs:rhCorticalWhiteMatterVol>");
			header--;
		}
		if (_Measures_volumetric_corticalwhitemattervol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:CorticalWhiteMatterVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_corticalwhitemattervol,"float"));
			writer.write("</fs:CorticalWhiteMatterVol>");
			header--;
		}
		if (_Measures_volumetric_brainsegvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:BrainSegVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_brainsegvol,"float"));
			writer.write("</fs:BrainSegVol>");
			header--;
		}
		if (_Measures_volumetric_brainsegvolnotvent!=null){
			writer.write("\n" + createHeader(header++) + "<fs:BrainSegVolNotVent");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_brainsegvolnotvent,"float"));
			writer.write("</fs:BrainSegVolNotVent>");
			header--;
		}
		if (_Measures_volumetric_brainsegvolnotventsurf!=null){
			writer.write("\n" + createHeader(header++) + "<fs:BrainSegVolNotVentSurf");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_brainsegvolnotventsurf,"float"));
			writer.write("</fs:BrainSegVolNotVentSurf>");
			header--;
		}
		if (_Measures_volumetric_supratentorialvolnotvent!=null){
			writer.write("\n" + createHeader(header++) + "<fs:SupraTentorialVolNotVent");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_supratentorialvolnotvent,"float"));
			writer.write("</fs:SupraTentorialVolNotVent>");
			header--;
		}
		if (_Measures_volumetric_supratentorialvolnotventvox!=null){
			writer.write("\n" + createHeader(header++) + "<fs:SupraTentorialVolNotVentVox");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_supratentorialvolnotventvox,"float"));
			writer.write("</fs:SupraTentorialVolNotVentVox>");
			header--;
		}
		if (_Measures_volumetric_maskvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:MaskVol");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_maskvol,"float"));
			writer.write("</fs:MaskVol>");
			header--;
		}
		if (_Measures_volumetric_brainsegvolToEtiv!=null){
			writer.write("\n" + createHeader(header++) + "<fs:BrainSegVol-to-eTIV");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_brainsegvolToEtiv,"float"));
			writer.write("</fs:BrainSegVol-to-eTIV>");
			header--;
		}
		if (_Measures_volumetric_maskvolToEtiv!=null){
			writer.write("\n" + createHeader(header++) + "<fs:MaskVol-to-eTIV");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_maskvolToEtiv,"float"));
			writer.write("</fs:MaskVol-to-eTIV>");
			header--;
		}
		if (_Measures_volumetric_lhsurfaceholes!=null){
			writer.write("\n" + createHeader(header++) + "<fs:lhSurfaceHoles");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_lhsurfaceholes,"float"));
			writer.write("</fs:lhSurfaceHoles>");
			header--;
		}
		if (_Measures_volumetric_rhsurfaceholes!=null){
			writer.write("\n" + createHeader(header++) + "<fs:rhSurfaceHoles");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_rhsurfaceholes,"float"));
			writer.write("</fs:rhSurfaceHoles>");
			header--;
		}
		if (_Measures_volumetric_surfaceholes!=null){
			writer.write("\n" + createHeader(header++) + "<fs:SurfaceHoles");
			writer.write(">");
			writer.write(ValueParser(_Measures_volumetric_surfaceholes,"float"));
			writer.write("</fs:SurfaceHoles>");
			header--;
		}
			int child2=0;
			int att2=0;
			child2+=_Measures_volumetric_regions_region.size();
			if(child2>0 || att2>0){
				writer.write("\n" + createHeader(header++) + "<fs:regions");
			if(child2==0){
				writer.write("/>");
			}else{
				writer.write(">");
		//REFERENCE FROM fsData -> measures/volumetric/regions/region
		java.util.Iterator iter3=_Measures_volumetric_regions_region.iterator();
		while(iter3.hasNext()){
			org.nrg.xdat.bean.FsFsdataRegionBean child = (org.nrg.xdat.bean.FsFsdataRegionBean)iter3.next();
			writer.write("\n" + createHeader(header++) + "<fs:region");
			child.addXMLAtts(writer);
			if(!child.getFullSchemaElementName().equals("fs:fsData_region")){
				writer.write(" xsi:type=\"" + child.getFullSchemaElementName() + "\"");
			}
			if (child.hasXMLBodyContent()){
				writer.write(">");
				boolean return4 =child.addXMLBody(writer,header);
				if(return4){
					writer.write("\n" + createHeader(--header) + "</fs:region>");
				}else{
					writer.write("</fs:region>");
					header--;
				}
			}else {writer.write("/>");header--;}
		}
				writer.write("\n" + createHeader(--header) + "</fs:regions>");
			}
			}

				writer.write("\n" + createHeader(--header) + "</fs:volumetric>");
			}
			}

			int child4=0;
			int att4=0;
			child4+=_Measures_surface_hemisphere.size();
			if(child4>0 || att4>0){
				writer.write("\n" + createHeader(header++) + "<fs:surface");
			if(child4==0){
				writer.write("/>");
			}else{
				writer.write(">");
		//REFERENCE FROM fsData -> measures/surface/hemisphere
		java.util.Iterator iter5=_Measures_surface_hemisphere.iterator();
		while(iter5.hasNext()){
			org.nrg.xdat.bean.FsFsdataHemisphereBean child = (org.nrg.xdat.bean.FsFsdataHemisphereBean)iter5.next();
			writer.write("\n" + createHeader(header++) + "<fs:hemisphere");
			child.addXMLAtts(writer);
			if(!child.getFullSchemaElementName().equals("fs:fsData_hemisphere")){
				writer.write(" xsi:type=\"" + child.getFullSchemaElementName() + "\"");
			}
			if (child.hasXMLBodyContent()){
				writer.write(">");
				boolean return6 =child.addXMLBody(writer,header);
				if(return6){
					writer.write("\n" + createHeader(--header) + "</fs:hemisphere>");
				}else{
					writer.write("</fs:hemisphere>");
					header--;
				}
			}else {writer.write("/>");header--;}
		}
				writer.write("\n" + createHeader(--header) + "</fs:surface>");
			}
			}

				writer.write("\n" + createHeader(--header) + "</fs:measures>");
			}
			}

	return true;
	}


	protected boolean hasXMLBodyContent(){
		if (_FsVersion!=null) return true;
		if (_IlpAge!=null) return true;
			if(_Measures_volumetric_icv!=null) return true;
			if(_Measures_volumetric_rhcortexvol!=null) return true;
			if(_Measures_volumetric_lhcorticalwhitemattervol!=null) return true;
			if(_Measures_volumetric_supratentorialvolnotventvox!=null) return true;
			if(_Measures_volumetric_supratentorialvolnotvent!=null) return true;
			if(_Measures_volumetric_surfaceholes!=null) return true;
			if(_Measures_volumetric_subcortgrayvol!=null) return true;
			if(_Measures_volumetric_brainsegvolnotventsurf!=null) return true;
			if(_Measures_volumetric_corticalwhitemattervol!=null) return true;
			if(_Measures_volumetric_brainsegvolnotvent!=null) return true;
			if(_Measures_volumetric_lhsurfaceholes!=null) return true;
			if(_Measures_volumetric_lhcortexvol!=null) return true;
			if(_Measures_volumetric_rhsurfaceholes!=null) return true;
			if(_Measures_surface_hemisphere.size()>0)return true;
			if(_Measures_volumetric_regions_region.size()>0)return true;
			if(_Measures_volumetric_cortexvol!=null) return true;
			if(_Measures_volumetric_maskvolToEtiv!=null) return true;
			if(_Measures_volumetric_rhcorticalwhitemattervol!=null) return true;
			if(_Measures_volumetric_maskvol!=null) return true;
			if(_Measures_volumetric_brainsegvol!=null) return true;
			if(_Measures_volumetric_totalgrayvol!=null) return true;
			if(_Measures_volumetric_brainsegvolToEtiv!=null) return true;
			if(_Measures_volumetric_supratentorialvol!=null) return true;
		if(super.hasXMLBodyContent())return true;
		return false;
	}
}
