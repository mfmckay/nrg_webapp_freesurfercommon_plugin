/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:48 CST 2017
 *
 */
package org.nrg.xdat.model;

import java.util.List;

/**
 * @author XDAT
 *
 */
public interface FsAsegregionanalysisI extends XnatMrassessordataI {

	public String getXSIType();

	public void toXML(java.io.Writer writer) throws java.lang.Exception;

	/**
	 * @return Returns the BrainMaskNVox.
	 */
	public Double getBrainmasknvox();

	/**
	 * Sets the value for BrainMaskNVox.
	 * @param v Value to Set.
	 */
	public void setBrainmasknvox(Double v);

	/**
	 * @return Returns the BrainMaskVol.
	 */
	public Double getBrainmaskvol();

	/**
	 * Sets the value for BrainMaskVol.
	 * @param v Value to Set.
	 */
	public void setBrainmaskvol(Double v);

	/**
	 * @return Returns the BrainSegNVox.
	 */
	public Double getBrainsegnvox();

	/**
	 * Sets the value for BrainSegNVox.
	 * @param v Value to Set.
	 */
	public void setBrainsegnvox(Double v);

	/**
	 * @return Returns the BrainSegVol.
	 */
	public Double getBrainsegvol();

	/**
	 * Sets the value for BrainSegVol.
	 * @param v Value to Set.
	 */
	public void setBrainsegvol(Double v);

	/**
	 * @return Returns the ICV.
	 */
	public Double getIcv();

	/**
	 * Sets the value for ICV.
	 * @param v Value to Set.
	 */
	public void setIcv(Double v);

	/**
	 * regions/region
	 * @return Returns an List of org.nrg.xdat.model.FsAsegregionanalysisRegionI
	 */
	public <A extends org.nrg.xdat.model.FsAsegregionanalysisRegionI> List<A> getRegions_region();

	/**
	 * regions/region
	 * @return Returns an List of org.nrg.xdat.model.FsAsegregionanalysisRegionI
	 */
	public <A extends org.nrg.xdat.model.FsAsegregionanalysisRegionI> void addRegions_region(A item) throws Exception;
}
