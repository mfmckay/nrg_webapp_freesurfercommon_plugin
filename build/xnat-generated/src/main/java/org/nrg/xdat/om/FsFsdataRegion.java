/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:47 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 *//*
 ******************************** 
 * DO NOT MODIFY THIS FILE HERE
 *
 * TO MODIFY, COPY THIS FILE to src/main/java/org/nrg/xdat/om/ and modify it there 
 ********************************/
@SuppressWarnings({"unchecked","rawtypes"})
public class FsFsdataRegion extends BaseFsFsdataRegion {

	public FsFsdataRegion(ItemI item)
	{
		super(item);
	}

	public FsFsdataRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsFsdataRegion(UserI user)
	 **/
	public FsFsdataRegion()
	{}

	public FsFsdataRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
