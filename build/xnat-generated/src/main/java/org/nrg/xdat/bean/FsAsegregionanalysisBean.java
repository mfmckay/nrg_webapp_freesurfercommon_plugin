/*
 * GENERATED FILE
 * Created on Wed Jan 25 12:16:48 CST 2017
 *
 */
package org.nrg.xdat.bean;
import org.apache.log4j.Logger;
import org.nrg.xdat.bean.base.BaseElement;

import java.util.*;

/**
 * @author XDAT
 *
 *//*
 ******************************** 
 * DO NOT MODIFY THIS FILE 
 *
 ********************************/
@SuppressWarnings({"unchecked","rawtypes"})
public class FsAsegregionanalysisBean extends XnatMrassessordataBean implements java.io.Serializable, org.nrg.xdat.model.FsAsegregionanalysisI {
	public static final Logger logger = Logger.getLogger(FsAsegregionanalysisBean.class);
	public static final String SCHEMA_ELEMENT_NAME="fs:asegRegionAnalysis";

	public String getSchemaElementName(){
		return "asegRegionAnalysis";
	}

	public String getFullSchemaElementName(){
		return "fs:asegRegionAnalysis";
	}

	//FIELD

	private Double _Brainmasknvox=null;

	/**
	 * @return Returns the BrainMaskNVox.
	 */
	public Double getBrainmasknvox() {
		return _Brainmasknvox;
	}

	/**
	 * Sets the value for BrainMaskNVox.
	 * @param v Value to Set.
	 */
	public void setBrainmasknvox(Double v){
		_Brainmasknvox=v;
	}

	/**
	 * Sets the value for BrainMaskNVox.
	 * @param v Value to Set.
	 */
	public void setBrainmasknvox(String v)  {
		_Brainmasknvox=formatDouble(v);
	}

	//FIELD

	private Double _Brainmaskvol=null;

	/**
	 * @return Returns the BrainMaskVol.
	 */
	public Double getBrainmaskvol() {
		return _Brainmaskvol;
	}

	/**
	 * Sets the value for BrainMaskVol.
	 * @param v Value to Set.
	 */
	public void setBrainmaskvol(Double v){
		_Brainmaskvol=v;
	}

	/**
	 * Sets the value for BrainMaskVol.
	 * @param v Value to Set.
	 */
	public void setBrainmaskvol(String v)  {
		_Brainmaskvol=formatDouble(v);
	}

	//FIELD

	private Double _Brainsegnvox=null;

	/**
	 * @return Returns the BrainSegNVox.
	 */
	public Double getBrainsegnvox() {
		return _Brainsegnvox;
	}

	/**
	 * Sets the value for BrainSegNVox.
	 * @param v Value to Set.
	 */
	public void setBrainsegnvox(Double v){
		_Brainsegnvox=v;
	}

	/**
	 * Sets the value for BrainSegNVox.
	 * @param v Value to Set.
	 */
	public void setBrainsegnvox(String v)  {
		_Brainsegnvox=formatDouble(v);
	}

	//FIELD

	private Double _Brainsegvol=null;

	/**
	 * @return Returns the BrainSegVol.
	 */
	public Double getBrainsegvol() {
		return _Brainsegvol;
	}

	/**
	 * Sets the value for BrainSegVol.
	 * @param v Value to Set.
	 */
	public void setBrainsegvol(Double v){
		_Brainsegvol=v;
	}

	/**
	 * Sets the value for BrainSegVol.
	 * @param v Value to Set.
	 */
	public void setBrainsegvol(String v)  {
		_Brainsegvol=formatDouble(v);
	}

	//FIELD

	private Double _Icv=null;

	/**
	 * @return Returns the ICV.
	 */
	public Double getIcv() {
		return _Icv;
	}

	/**
	 * Sets the value for ICV.
	 * @param v Value to Set.
	 */
	public void setIcv(Double v){
		_Icv=v;
	}

	/**
	 * Sets the value for ICV.
	 * @param v Value to Set.
	 */
	public void setIcv(String v)  {
		_Icv=formatDouble(v);
	}
	 private List<org.nrg.xdat.bean.FsAsegregionanalysisRegionBean> _Regions_region =new ArrayList<org.nrg.xdat.bean.FsAsegregionanalysisRegionBean>();

	/**
	 * regions/region
	 * @return Returns an List of org.nrg.xdat.bean.FsAsegregionanalysisRegionBean
	 */
	public <A extends org.nrg.xdat.model.FsAsegregionanalysisRegionI> List<A> getRegions_region() {
		return (List<A>) _Regions_region;
	}

	/**
	 * Sets the value for regions/region.
	 * @param v Value to Set.
	 */
	public void setRegions_region(ArrayList<org.nrg.xdat.bean.FsAsegregionanalysisRegionBean> v){
		_Regions_region=v;
	}

	/**
	 * Adds the value for regions/region.
	 * @param v Value to Set.
	 */
	public void addRegions_region(org.nrg.xdat.bean.FsAsegregionanalysisRegionBean v){
		_Regions_region.add(v);
	}

	/**
	 * regions/region
	 * Adds org.nrg.xdat.model.FsAsegregionanalysisRegionI
	 */
	public <A extends org.nrg.xdat.model.FsAsegregionanalysisRegionI> void addRegions_region(A item) throws Exception{
	_Regions_region.add((org.nrg.xdat.bean.FsAsegregionanalysisRegionBean)item);
	}

	/**
	 * Adds the value for regions/region.
	 * @param v Value to Set.
	 */
	public void addRegions_region(Object v){
		if (v instanceof org.nrg.xdat.bean.FsAsegregionanalysisRegionBean)
			_Regions_region.add((org.nrg.xdat.bean.FsAsegregionanalysisRegionBean)v);
		else
			throw new IllegalArgumentException("Must be a valid org.nrg.xdat.bean.FsAsegregionanalysisRegionBean");
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public void setDataField(String xmlPath,String v) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("BrainMaskNVox")){
			setBrainmasknvox(v);
		}else if (xmlPath.equals("BrainMaskVol")){
			setBrainmaskvol(v);
		}else if (xmlPath.equals("BrainSegNVox")){
			setBrainsegnvox(v);
		}else if (xmlPath.equals("BrainSegVol")){
			setBrainsegvol(v);
		}else if (xmlPath.equals("ICV")){
			setIcv(v);
		}
		else{
			super.setDataField(xmlPath,v);
		}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public void setReferenceField(String xmlPath,BaseElement v) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("regions/region")){
			addRegions_region(v);
		}
		else{
			super.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public Object getDataFieldValue(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("BrainMaskNVox")){
			return getBrainmasknvox();
		}else if (xmlPath.equals("BrainMaskVol")){
			return getBrainmaskvol();
		}else if (xmlPath.equals("BrainSegNVox")){
			return getBrainsegnvox();
		}else if (xmlPath.equals("BrainSegVol")){
			return getBrainsegvol();
		}else if (xmlPath.equals("ICV")){
			return getIcv();
		}
		else{
			return super.getDataFieldValue(xmlPath);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public Object getReferenceField(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("regions/region")){
			return getRegions_region();
		}
		else{
			return super.getReferenceField(xmlPath);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	public String getReferenceFieldName(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("regions/region")){
			return "http://nrg.wustl.edu/fs:asegRegionAnalysis_region";
		}
		else{
			return super.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	public String getFieldType(String xmlPath) throws BaseElement.UnknownFieldException{
		if (xmlPath.equals("BrainMaskNVox")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("BrainMaskVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("BrainSegNVox")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("BrainSegVol")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("ICV")){
			return BaseElement.field_data;
		}else if (xmlPath.equals("regions/region")){
			return BaseElement.field_multi_reference;
		}
		else{
			return super.getFieldType(xmlPath);
		}
	}

	/**
	 * Returns arraylist of all fields
	 */
	public ArrayList getAllFields() {
		ArrayList all_fields=new ArrayList();
		all_fields.add("BrainMaskNVox");
		all_fields.add("BrainMaskVol");
		all_fields.add("BrainSegNVox");
		all_fields.add("BrainSegVol");
		all_fields.add("ICV");
		all_fields.add("regions/region");
		all_fields.addAll(super.getAllFields());
		return all_fields;
	}


	public String toString(){
		java.io.StringWriter sw = new java.io.StringWriter();
		try{this.toXML(sw,true);}catch(java.io.IOException e){}
		return sw.toString();
	}


	public void toXML(java.io.Writer writer,boolean prettyPrint) throws java.io.IOException{
		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		writer.write("\n<fs:ASEGRegionAnalysis");
		TreeMap map = new TreeMap();
		map.putAll(getXMLAtts());
		map.put("xmlns:arc","http://nrg.wustl.edu/arc");
		map.put("xmlns:cat","http://nrg.wustl.edu/catalog");
		map.put("xmlns:fs","http://nrg.wustl.edu/fs");
		map.put("xmlns:pipe","http://nrg.wustl.edu/pipe");
		map.put("xmlns:prov","http://www.nbirn.net/prov");
		map.put("xmlns:scr","http://nrg.wustl.edu/scr");
		map.put("xmlns:val","http://nrg.wustl.edu/val");
		map.put("xmlns:wrk","http://nrg.wustl.edu/workflow");
		map.put("xmlns:xdat","http://nrg.wustl.edu/security");
		map.put("xmlns:xnat","http://nrg.wustl.edu/xnat");
		map.put("xmlns:xnat_a","http://nrg.wustl.edu/xnat_assessments");
		map.put("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		java.util.Iterator iter =map.keySet().iterator();
		while(iter.hasNext()){
			String key = (String)iter.next();
			writer.write(" " + key + "=\"" + map.get(key) + "\"");
		}
		int header = 0;
		if (prettyPrint)header++;
		writer.write(">");
		addXMLBody(writer,header);
		if (prettyPrint)header--;
		writer.write("\n</fs:ASEGRegionAnalysis>");
	}


	protected void addXMLAtts(java.io.Writer writer) throws java.io.IOException{
		TreeMap map = this.getXMLAtts();
		java.util.Iterator iter =map.keySet().iterator();
		while(iter.hasNext()){
			String key = (String)iter.next();
			writer.write(" " + key + "=\"" + map.get(key) + "\"");
		}
	}


	protected TreeMap getXMLAtts() {
		TreeMap map = super.getXMLAtts();
		return map;
	}


	protected boolean addXMLBody(java.io.Writer writer, int header) throws java.io.IOException{
		super.addXMLBody(writer,header);
		//REFERENCE FROM asegRegionAnalysis -> mrAssessorData
		if (_Brainmasknvox!=null){
			writer.write("\n" + createHeader(header++) + "<fs:BrainMaskNVox");
			writer.write(">");
			writer.write(ValueParser(_Brainmasknvox,"float"));
			writer.write("</fs:BrainMaskNVox>");
			header--;
		}
		if (_Brainmaskvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:BrainMaskVol");
			writer.write(">");
			writer.write(ValueParser(_Brainmaskvol,"float"));
			writer.write("</fs:BrainMaskVol>");
			header--;
		}
		if (_Brainsegnvox!=null){
			writer.write("\n" + createHeader(header++) + "<fs:BrainSegNVox");
			writer.write(">");
			writer.write(ValueParser(_Brainsegnvox,"float"));
			writer.write("</fs:BrainSegNVox>");
			header--;
		}
		if (_Brainsegvol!=null){
			writer.write("\n" + createHeader(header++) + "<fs:BrainSegVol");
			writer.write(">");
			writer.write(ValueParser(_Brainsegvol,"float"));
			writer.write("</fs:BrainSegVol>");
			header--;
		}
		if (_Icv!=null){
			writer.write("\n" + createHeader(header++) + "<fs:ICV");
			writer.write(">");
			writer.write(ValueParser(_Icv,"float"));
			writer.write("</fs:ICV>");
			header--;
		}
			int child0=0;
			int att0=0;
			child0+=_Regions_region.size();
			if(child0>0 || att0>0){
				writer.write("\n" + createHeader(header++) + "<fs:regions");
			if(child0==0){
				writer.write("/>");
			}else{
				writer.write(">");
		//REFERENCE FROM asegRegionAnalysis -> regions/region
		java.util.Iterator iter1=_Regions_region.iterator();
		while(iter1.hasNext()){
			org.nrg.xdat.bean.FsAsegregionanalysisRegionBean child = (org.nrg.xdat.bean.FsAsegregionanalysisRegionBean)iter1.next();
			writer.write("\n" + createHeader(header++) + "<fs:region");
			child.addXMLAtts(writer);
			if(!child.getFullSchemaElementName().equals("fs:asegRegionAnalysis_region")){
				writer.write(" xsi:type=\"" + child.getFullSchemaElementName() + "\"");
			}
			if (child.hasXMLBodyContent()){
				writer.write(">");
				boolean return2 =child.addXMLBody(writer,header);
				if(return2){
					writer.write("\n" + createHeader(--header) + "</fs:region>");
				}else{
					writer.write("</fs:region>");
					header--;
				}
			}else {writer.write("/>");header--;}
		}
				writer.write("\n" + createHeader(--header) + "</fs:regions>");
			}
			}

	return true;
	}


	protected boolean hasXMLBodyContent(){
		if (_Brainmasknvox!=null) return true;
		if (_Brainmaskvol!=null) return true;
		if (_Brainsegnvox!=null) return true;
		if (_Brainsegvol!=null) return true;
		if (_Icv!=null) return true;
			if(_Regions_region.size()>0)return true;
		if(super.hasXMLBodyContent())return true;
		return false;
	}
}
